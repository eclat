/^\+-/ { if (state < 3) state++; else state = -1; next }
/^\|/   { if (state == 1) state++; else state = -1; next }
state != 3 { next }
{ total++ }
$3=="[X]" { impl++; if ($4 != "[X]") print $1 " (" $2 ") not documented"; }
$4=="[X]" { doc++; }
END {
	if (!total)
		exit(0);
	printf("Total      : %4d\n", total);
	printf("Implemented: %4d / %3d%%\n", impl, impl * 100 / total);
	printf("Documented : %4d / %3d%%\n", doc, doc * 100 / total);
}
	
