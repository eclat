/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
#include "lstag-cl.h"

int
eclat_describe_tags(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	char *bufptr = NULL;
	size_t bufsize = 0;
	struct grecs_list_entry *ep;
	int n = 1;
	
	parse_options(env, argc, argv, &i);
	argv += i;
	argc -= i;
	translate_resource_ids(argc, argv);

	if (reslist->head) {
		eclat_request_add_param(env->request,
				      "Filter.1.Name", "resource-id");
		for (ep = reslist->head, i = 1; ep; ep = ep->next, i++) {
			struct resource *res = ep->data;
			if (res->map)
				translate_ids(1, &res->resid, res->map);
			grecs_asprintf(&bufptr, &bufsize,
				       "Filter.1.Value.%d", i);
			eclat_request_add_param(env->request, bufptr, res->resid);
		}
		n++;
	}
	describe_request_update(env, argc, argv, NULL, n, NULL);
	
	return 0;
}
