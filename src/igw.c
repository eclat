/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

int
eclat_create_internet_gateway(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	generic_proginfo->args_doc = NULL;
	generic_parse_options(env->cmd,
			      "create internet gateway",
			      argc, argv, &i);
	if (argc > i)
		die(EX_USAGE, "wrong number of arguments");
	return 0;
}

int
eclat_delete_internet_gateway(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "IGW-ID";
	generic_parse_options(env->cmd,
			      "delete internet gateway",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");

	translate_ids(1, argv, MAP_IGW);
	eclat_request_add_param(q, "InternetGatewayId", argv[0]);

	return 0;
}

static struct filter_descr filters[] = {
	{ "attachment.state", FILTER_STRING },
	{ "attachment.vpc-id", FILTER_STRING },
	{ "internet-gateway-id", FILTER_STRING },
	{ "tag-key", FILTER_STRING },
	{ "tag-value", FILTER_STRING },
	{ "tag:<KEY>", FILTER_STRING },
	{ NULL }
};	

int
eclat_describe_internet_gateways(eclat_command_env_t *env,
				 int argc, char **argv)
{
	int i;

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = "[FILTER...] [ID...]";
	generic_parse_options(env->cmd,
			      "describe internet gateways",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_IGW);

	describe_request_create(env, argc, argv, "InternetGatewayId");
	return 0;
}

int
eclat_attach_internet_gateway(eclat_command_env_t *env,
			      int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "IGW-ID VPC-ID";
	generic_parse_options(env->cmd,
			      "attach internet gateway to a VPC",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	if (argc != 2)
		die(EX_USAGE, "wrong number of arguments");
	
	translate_ids(1, argv, MAP_IGW);
	translate_ids(1, argv+1, MAP_VPC);

	eclat_request_add_param(q, "InternetGatewayId", argv[0]);
	eclat_request_add_param(q, "VpcId", argv[1]);

	return 0;
}

int
eclat_detach_internet_gateway(eclat_command_env_t *env,
			      int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "IGW-ID VPC-ID";
	generic_parse_options(env->cmd,
			      "detach internet gateway from VPC",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	if (argc != 2)
		die(EX_USAGE, "wrong number of arguments");
	
	translate_ids(1, argv, MAP_IGW);
	translate_ids(1, argv+1, MAP_VPC);

	eclat_request_add_param(q, "InternetGatewayId", argv[0]);
	eclat_request_add_param(q, "VpcId", argv[1]);

	return 0;
}	
