/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static void
parse_options(struct eclat_command const *env,
	      const char *docstring,
              int argc, char *argv[], int *index)
{
	generic_proginfo->args_doc = "ID [ID...]";
	return generic_parse_options(env, docstring, argc, argv, index);
}

static int
start_stop_instance(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	char buf[128], *bend;
	size_t bs;

	if (argc == 0)
		die(EX_USAGE, "no instance ids");
	translate_ids(argc, argv, MAP_INSTANCE);
	
	strcpy(buf, "InstanceId.");
	bend = buf + strlen(buf);
	bs = sizeof(buf) - strlen(buf);
	for (i = 0; i < argc; i++) {
		snprintf(bend, bs, "%lu", (unsigned long)(i + 1));
		eclat_request_add_param(q, buf, argv[i]);
	}
	return 0;	
}

int
eclat_start_instance(eclat_command_env_t *env, int argc, char **argv)
{
	int i;

	parse_options(env->cmd,
		      "Start named instances",
		      argc, argv, &i);

	debug(ECLAT_DEBCAT_MAIN, 1, ("starting instances"));
	return start_stop_instance(env, argc - i, argv + i);
}


static int force;

#include "stop-cl.h"

int
eclat_stop_instance(eclat_command_env_t *env, int argc, char **argv)
{
	int i;

	parse_stop_options(env, argc, argv, &i);
	debug(ECLAT_DEBCAT_MAIN, 1, ("stopping instances"));
	if (force)
		eclat_request_add_param(env->request, "Force", "1");
	return start_stop_instance(env, argc - i, argv + i);
}

int
eclat_reboot_instance(eclat_command_env_t *env, int argc, char **argv)
{
	int i;

	parse_options(env->cmd,
		      "Reboot named instances",
		      argc, argv, &i);

	debug(ECLAT_DEBCAT_MAIN, 1, ("rebooting instances"));
	return start_stop_instance(env, argc - i, argv + i);
}

int
eclat_terminate_instances(eclat_command_env_t *env, int argc, char **argv)
{
	int i;

	parse_options(env->cmd,
		      "Terminate named instances",
		      argc, argv, &i);

	debug(ECLAT_DEBCAT_MAIN, 1, ("terminate instances"));
	return start_stop_instance(env, argc - i, argv + i);
}
