/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
char *idname;
char *cmdname;
#include "lsattr-cl.h"

int
eclat_lsattr(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct eclat_command *command;

	parse_options(env, argc, argv, &i);

	argc -= i;
	argv += i;

	if (argc < 1)
		die(EX_USAGE, "wrong number of arguments");
	
	if (cmdname) {
		translate_ids(1, argv, idname);
	} else {
		if (strncmp (argv[0], "i-", 2) == 0)
			cmdname = "lsiattr";
		else if (strncmp (argv[0], "snap-", 5) == 0)
			cmdname = "lssattr";
		else
			die(EX_USAGE, "unrecognized resource type: %s", argv[0]);
	}

	command = find_command_name(cmdname);
	if (!command)
		abort();
	translation_enabled = 0;
	argv--;
	argc++;
	argv[0] = "lsattr";
	return eclat_do_command(env, command, argc, argv);
}
