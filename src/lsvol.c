/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static char *attachment_status_str[] = {
	"attaching", "attached", "detaching", "detached", NULL
};
static char *status_str[] = {
	"creating", "available", "in-use", "deleting", "deleted", "error", NULL
};
static char *volume_type_str[] = { "standard", "io1", NULL };
static struct filter_descr filters[] = {
	{ "attachment.attach-time", FILTER_DATE },
	{ "attachment.delete-on-termination", FILTER_BOOL },
	{ "attachment.device", FILTER_STRING },
	{ "attachment.instance-id", FILTER_STRING },
	{ "attachment.status", FILTER_ENUM, attachment_status_str },
	{ "availability-zone", FILTER_STRING },
	{ "create-time", FILTER_DATE },
	{ "size", FILTER_INT },
	{ "snapshot-id", FILTER_STRING },
	{ "status", FILTER_ENUM, status_str },
	{ "tag-key", FILTER_STRING },
	{ "tag-value", FILTER_STRING },
	{ "tag:<KEY>", FILTER_STRING },
	{ "volume-id", FILTER_STRING },
	{ "volume-type", FILTER_ENUM, volume_type_str },
	{ NULL }
};


int
eclat_describe_volumes(eclat_command_env_t *env, int argc, char **argv)
{
	int i;

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = "[FILTER...] [ID...]";
	generic_parse_options(env->cmd,
			      "describe Amazon EBS volumes",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_VOLUME);

	describe_request_create(env, argc, argv, "VolumeId");
	return 0;
}
