/* This file is part of Eclat.
   Copyright (C) 2015-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

int
eclat_describe_vpc_attribute(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	const char *attrname;
	static char *attrs[] = {
		"enableDnsSupport",
		"enableDnsHostnames",
		NULL
	};
	
	generic_proginfo->args_doc = "VPC-ID [ATTR]";
	available_attrs = attrs;
	generic_proginfo->print_help_hook = list_attrs;
	generic_parse_options(env->cmd,
			      "describe VPC attributes",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	
	switch (argc) {
	default:
		die(EX_USAGE, "wrong number of arguments");
	case 2:
		attrname = canonattrname(attrs, argv[1], NULL, NULL);
		if (!attrname)
			die(EX_USAGE, "unrecognized attribute name");
		break;
	case 1:
		attrname = attrs[0];
	}

	translate_ids(1, argv, MAP_VPC);
	eclat_request_add_param(q, "VpcId", argv[0]);
	if (attrname)
		eclat_request_add_param(q, "Attribute", attrname);
	return 0;
}

int
eclat_modify_vpc_attribute(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	const char *attrname;
	char *bufptr = NULL;
	size_t bufsize = 0;
	static char *attrs[] = {
		"EnableDnsSupport",
		"EnableDnsHostnames",
		NULL
	};

	
 	generic_proginfo->args_doc = "VPC-ID ATTR VALUE";
	available_attrs = attrs;
	generic_proginfo->print_help_hook = list_attrs;
	generic_parse_options(env->cmd,
			      "modify VPC attribute",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc != 3)
		die(EX_USAGE, "wrong number of arguments");
	attrname = canonattrname(attrs, argv[1], NULL, NULL);
	if (!attrname)
		die(EX_USAGE, "unrecognized attribute name");

	translate_ids(1, argv, MAP_VPC);
	eclat_request_add_param(q, "VpcId", argv[0]);
	grecs_asprintf(&bufptr, &bufsize, "%s.Value", attrname);
	eclat_request_add_param(q, bufptr, argv[2]);
	free(bufptr);
	return 0;
}
	
