/* This file is part of Eclat.
   Copyright (C) 2013-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>

static char *ami;
static char *instance_count = "1";
static struct grecs_list *secgrp, *devmap, *iface, *privip;
static char *keypair;
static char *user_data;
static char *type;
static char *zone;
static char *kernel;
static char *ramdisk;
static char *shutdown_behavior;
static char *placement_group;
static char *tenancy;
static char *subnet;
static char *private_ip;
static char *secipcount;
static char *profile_name;

static int monitor;
static int disable_term;
static int ebs_opt;

ECLAT_CL_BEGIN([<launch new instances>],
	       [<AMI-ID>])

OPTION(,n,[<N>],
       [<number of instances to launch>])
BEGIN
	/* FIXME: check if it's a valid number */
	instance_count = optarg;
END

OPTION(security-group,g,[<STRING>],
       [<The name of the security group>])
BEGIN
	if (!secgrp)
		secgrp = grecs_list_create();
	grecs_list_append(secgrp, optarg);
END

OPTION(keypair,k,[<ID>],
       [<Key pair name>])
BEGIN
	keypair = optarg;
END

OPTION(data,d,[<STRING>],
       [<user data to pass to the instance>])
BEGIN
	user_data = optarg;
END

OPTION(data-file,f,[<FILE>],
       [<read user data from FILE>])
BEGIN
	user_data = read_file(optarg);
END

OPTION(type,t,[<TYPE>],
       [<instance type>])
ALIAS(instance-type)
BEGIN
	type = optarg;
END

OPTION(zone,z,[<ZONE>],
       [<set availablility zone>])
ALIAS(availability-zone)
BEGIN
	zone = optarg;
END

OPTION(kernel,,[<ID>],
       [<select kernel ID>])
BEGIN
	kernel = optarg;
END

OPTION(ramdisk,,[<ID>],
       [<select ramdisk ID>])
BEGIN
	ramdisk = optarg;
END	

OPTION(devmap,m,[<DEV=SPEC>],
       [<set block device mapping>])
ALIAS(block-device-mapping)
BEGIN
	if (!devmap)
		devmap = grecs_list_create();
	grecs_list_append(devmap, optarg);
END

OPTION(monitor,,,
       [<enable monitoring>])
BEGIN
	monitor = 1;
END	

OPTION(disable-api-termination,,,
       [<disable API termination>])
BEGIN
	disable_term = 1;
END

OPTION(shutdown,,[<stop|terminate>],
       [<what to do on shutdown>])
ALIAS(instance-initiated-shutdown-behavior)       
BEGIN
	shutdown_behavior = optarg;
END

OPTION(placement-group,,[<NAME>],
       [<name of the placement group>])
BEGIN       
	placement_group = optarg;
END

OPTION(tenancy,,[<STRING>],
       [<placement tenancy>])
BEGIN
	tenancy = optarg;
END	

OPTION(subnet,s,[<ID>],
       [<subnet to launch the instance into>])
BEGIN
	subnet = optarg;
END	

OPTION(private-ip-address,,[<IP>],
       [<assign a specific private IP to the VPC instance>])
BEGIN
	private_ip = optarg;
END

dnl [--client-token token]

OPTION(network-interface,a,[<IFACE>],
       [<specify the network attachment for a VPC instance>])
ALIAS(iface)
BEGIN
	if (!iface)
		iface = grecs_list_create();
	grecs_list_append(iface, optarg);
END

OPTION(secondary-ip,,[<IP>],
       [<assign the IP as a secondary private IP address>])
ALIAS(secondary-private-ip-address)
BEGIN
	if (!privip)
		privip = grecs_list_create();
	grecs_list_append(privip, privip);
END

OPTION(number-secondary-ip,,[<N>],
       [<number of secondary IP addresses to assign>])
ALIAS(secondary-private-ip-address-count)
BEGIN
	secipcount = optarg;
END

OPTION(iam-profile,p,[<NAME>],
       [<IAM instance profile to associate with the launched instances>])
BEGIN
	profile_name = optarg;
END	

OPTION(ebs-optimized,,,
       [<optimize the instances for EBS I/O>])
BEGIN
	ebs_opt = 1;
END

ECLAT_CL_END

ECLAT_CL_PARSER(parse_options, [<int argc, char *argv[]>],[<
{
	int idx;

	GETOPT(argc, argv, idx, exit(EX_USAGE))
	if (idx != argc-1)
		die(EX_USAGE, "bad number of arguments");
	translate_ids(1, argv+idx, "ImageId");
	ami = argv[idx];
}
>])
