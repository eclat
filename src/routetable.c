/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

int
eclat_create_route_table(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	generic_proginfo->args_doc = "VPC-ID";
	generic_parse_options(env->cmd,
			      "create route table",
			      argc, argv, &i);
	argc -= i;
	argv += i;
	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");
	translate_ids(1, argv, MAP_VPC);
	eclat_request_add_param(q, "VpcId", argv[0]);
	return 0;
}

int
eclat_delete_route_table(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "RTAB-ID";
	generic_parse_options(env->cmd,
			      "delete route table",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");

//	translate_ids(1, argv, MAP_ROUTE_TABLE);
	eclat_request_add_param(q, "RouteTableId", argv[0]);

	return 0;
}

static struct filter_descr rtab_filters[] = {
	{ "association.route-table-association-id", FILTER_STRING },
	{ "association.route-table-id", FILTER_STRING },
	{ "association.subnet-id", FILTER_STRING },
	{ "association.main", FILTER_BOOL },
	{ "route-table-id", FILTER_STRING },
	{ "route.destination-cidr-block", FILTER_STRING },
	{ "route.destination-prefix-list-id", FILTER_STRING },
	{ "route.gateway-id", FILTER_STRING },
	{ "route.instance-id", FILTER_STRING },
	{ "route.origin", FILTER_STRING },
	{ "route.state", FILTER_STRING },
	{ "route.vpc-peering-connection-id", FILTER_STRING },
	{ "tag-key", FILTER_STRING },
	{ "tag-value", FILTER_STRING },
	{ "tag:<KEY>", FILTER_STRING },
	{ "vpc-id", FILTER_STRING },
	{ NULL }
};

int
eclat_describe_route_tables(eclat_command_env_t *env,
			    int argc, char **argv)
{
	int i;

	available_filters = rtab_filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = "[FILTER...] [ID...]";
	generic_parse_options(env->cmd,
			      "describe route tables",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_ROUTE_TABLE);

	describe_request_create(env, argc, argv, "RouteTableId");
	return 0;
}

int
eclat_associate_route_table(eclat_command_env_t *env,
			    int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "RTAB-ID SUBNET-ID";
	generic_parse_options(env->cmd,
			      "associate a subnet with a route table",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	if (argc != 2)
		die(EX_USAGE, "wrong number of arguments");
	
        translate_ids(1, argv, MAP_ROUTE_TABLE);
	translate_ids(1, argv+1, MAP_SUBNET);
	
	eclat_request_add_param(q, "RouteTableId", argv[0]);
	eclat_request_add_param(q, "SubnetId", argv[1]);

	return 0;
}

int
eclat_disassociate_route_table(eclat_command_env_t *env,
			       int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "RTBASSOC-ID";
	generic_parse_options(env->cmd,
			      "disassociate a subnet from a route table",
			      argc, argv, &i);
	argv += i;
	argc -= i;
	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");
	eclat_request_add_param(q, "AssociationId", argv[0]);
	return 0;
}

static void
route_create_request(struct ec2_request *req, int argc, char **argv)
{
	char const *id;
	static struct idtab {
		char *name;
		char *id;
	} idtab[] = {
		{ "gw", "GatewayId" },
		{ "inst", "InstanceId" },
		{ "if", "NetworkInterfaceId" },
		{ "peer", "VpcPeeringConnectionId" },
		{ NULL }
	};
	struct idtab *p;
		
	if (argc != 2)
		die(EX_USAGE, "wrong number of arguments");
	id = argv[0];

	for (p = idtab; p->name; p++)
		if (strcmp (id, p->name) == 0) {
			id = p->id;
			break;
		}

	translate_ids(1, argv+1, id);
	eclat_request_add_param(req, id, argv[1]);
}

int
eclat_route(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	char *command;
	char *rtb_id;
	char *op;
	char *cidr;
	
	generic_proginfo->args_doc = "RTB-ID OP CIDR [DEST ID]";
	generic_parse_options(env->cmd,
			      "manipulate the route table",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc < 3)
		die(EX_USAGE, "wrong number of arguments");

	translate_ids(1, argv, MAP_ROUTE_TABLE);

	rtb_id = argv[0];
	op = argv[1];
	cidr = argv[2];

	argc -= 3;
	argv += 3;
	
	if (strcmp(op, "add") == 0) {
		command = "CreateRoute";
		route_create_request(q, argc, argv);
	} else if (strcmp(op, "del") == 0) {
		command = "DeleteRoute";
		if (argc != 0)
			die(EX_USAGE, "wrong number of arguments");
	} else if (strcmp(op, "chg") == 0 || strcmp(op, "replace") == 0) {
		command = "ReplaceRoute";
		route_create_request(q, argc, argv);
	} else {
		die(EX_USAGE, "unrecognized operation %s", op);
	}

	eclat_request_add_param(q, "Action", command);
	eclat_request_add_param(q, "RouteTableId", rtb_id);
	eclat_request_add_param(q, "DestinationCidrBlock", cidr);
	return 0;
}
