/* This file is part of Eclat.
   Copyright (C) 2013-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#define DIR_INGRESS 0
#define DIR_EGRESS  1

static char const *authorize_comtab[] = {
	"AuthorizeSecurityGroupIngress",
	"AuthorizeSecurityGroupEgress"
};
static char const *revoke_comtab[] = {
	"RevokeSecurityGroupIngress",
	"RevokeSecurityGroupEgress"
};
static int direction = DIR_INGRESS;
   
ECLAT_CL_BEGIN([<modify ingress rules of a security group>],
	       [<[GROUPARG]>])

GROUP(Direction)
OPTION(input,I,,
       [<input (ingress)>])
ALIAS(ingress)       
BEGIN
       direction = DIR_INGRESS;
END

OPTION(output,O,,
       [<output (egress)>])
ALIAS(egress)
BEGIN
       direction = DIR_EGRESS;
END
	       
GROUP(Commands)	       
OPTION(add,A,,
       [<add rules>])
BEGIN
       command = authorize_comtab[direction];
END

OPTION(delete,D,,
       [<delete rules>])
BEGIN
       command = revoke_comtab[direction];
END

OPTION(list,L,,
       [<list rules>])
BEGIN
       list_option = 1;
       command = "DescribeSecurityGroups";
END       

GROUP(Rule constituents)

OPTION(protocol,p,[<PROTO>],
       [<IP protocol name or number>])
BEGIN
	proto = optarg;
END

OPTION(group-id,g,[<ID>],
       [<source group ID>])
BEGIN
	struct group_arg *g = grecs_malloc(sizeof(*g));
	translate_ids(1, &optarg, groupkw[GROUP_ID].map);
        g->type = GROUP_ID;
        g->str = optarg;
        g->usr = user;
        if (!group_list)
		group_list = _grecs_simple_list_create(1);
        grecs_list_append(group_list, g);
END

OPTION(group-name,G,[<NAME>],
       [<source group name>])
BEGIN
	struct group_arg *g = grecs_malloc(sizeof(*g));
	translate_ids(1, &optarg, groupkw[GROUP_NAME].map);
        g->type = GROUP_NAME;
        g->str = optarg;
        g->usr = user;
        if (!group_list)
		group_list = _grecs_simple_list_create(1);
        grecs_list_append(group_list, g);
END

OPTION(user,u,[<USER>],
       [<user name for the subsequent --group-name or --group-id option>])
BEGIN
	user = optarg;
END

OPTION(source,s,[<CIDR>],
       [<source CIDR>])
BEGIN
	if (!source_list)
		source_list = grecs_list_create();
        grecs_list_append(source_list, optarg);
END

OPTION(port,P,[<PORT[-PORT]>],
       [<destination port number or range>])
BEGIN
	char *p = strchr(optarg, '-');
        if (p && p > optarg) {
		*p++ = 0;
		to_port = p;
	} else
		to_port = optarg;
        from_port = optarg;
END

GROUP(Modifiers)
OPTION(name,n,,
       [<the GROUPARG argument is a group name>])
BEGIN
       dest_n = GROUP_NAME;
END

OPTION(next,N,,
       [<delimits rules>])
ALIAS(new)
BEGIN
	flush_rule();
END

ECLAT_CL_END

ECLAT_CL_PARSER(parse_options, [<int argc, char *argv[]>],[<
{
	int idx;

	GETOPT(argc, argv, idx, exit(EX_USAGE))
	argc -= idx;
	argv += idx;

	if (!command)
		die(EX_USAGE, "either --list or --add or --delete must be given");
	if (list_option) { 
		if (rule_n > 1 || group_list || source_list ||
		    from_port || to_port)
			die(EX_USAGE, "conflicting options");
		if (argc > 1)
			die(EX_USAGE, "bad number of arguments");
	} else if (argc != 1)
		die(EX_USAGE, "bad number of arguments");
	translate_ids(argc, argv, groupkw[dest_n].map);

	eclat_request_add_param(env->request, "Action", command);
	if (list_option)
		describe_request_create(env, argc, argv, groupkw[dest_n].resid);
	else {
		if (group_list || source_list || from_port || to_port || proto)
			flush_rule();
		if (rule_n == 1)
			die(EX_USAGE, "no rules");

		eclat_request_add_param(request, groupkw[dest_n].resid, argv[0]);
	}
}>])
