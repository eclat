/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
#include "lssnap-cl.h"

static char *snapshot_status_str[] = {
	"pending", "completed", "error", NULL
};

static struct filter_descr filters[] = {
	{ "description", FILTER_STRING },
	{ "owner-alias", FILTER_STRING },
	{ "owner-id", FILTER_STRING },
	{ "progress", FILTER_STRING },
	{ "snapshot-id", FILTER_STRING },
	{ "start-time", FILTER_DATE },
	{ "status", FILTER_ENUM, snapshot_status_str },
	{ "tag-key", FILTER_STRING },
	{ "tag-value", FILTER_STRING },
	{ "tag:<KEY>", FILTER_STRING },
	{ "volume-id", FILTER_STRING },
	{ "volume-size", FILTER_STRING },
	{ NULL }
};
	
	
int
eclat_describe_snapshots(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct grecs_list_entry *ep;
	char *buf = NULL;
	size_t size = 0;

	available_filters = filters;
	proginfo.print_help_hook = list_filters;
	parse_options(env, argc, argv, &i);
	argc -= i;	
	argv += i;
	translate_ids(argc, argv, MAP_SNAPSHOT);

	describe_request_create(env, argc, argv, "SnapshotId");
	
	if (parmlist) {
		for (ep = parmlist->head; ep; ep = ep->next) {
			struct param *param = ep->data;
			grecs_asprintf(&buf, &size, "%s.%u",
				       param->name, param->idx);
			eclat_request_add_param(env->request, buf, param->value);
		}
	}
	grecs_list_free(parmlist);
			
	return 0;
}
			
	

	
