/* This file is part of Eclat.
   Copyright (C) 2015-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
#include "mkvpc-cl.h"

int
eclat_create_vpc(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	
	parse_options(env, argc, argv, &i);
	argc -= i;
	argv += i;
	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");

	if (vpc_tenancy)
		eclat_request_add_param(q, "InstanceTenancy", "true");
	eclat_request_add_param(q, "CidrBlock", argv[0]);
	return 0;
}
	

