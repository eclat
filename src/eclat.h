/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <sysexits.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <curl/curl.h>
#include <expat.h>
#include "grecs.h"
#include "grecs/opt.h"
#include "wordsplit.h"
#include "libeclat.h"
#include "forlan.h"

#define EX_CANCELLED 16

#define	ECLAT_DEBCAT_MAIN    0
#define ECLAT_DEBCAT_CFGRAM  1
#define ECLAT_DEBCAT_CFLEX   2
#define ECLAT_DEBCAT_CONF    3
#define ECLAT_DEBCAT_CURL    4
#define ECLAT_DEBCAT_FORLAN  5
#define ECLAT_DEBCAT_DUMP    6

enum authentication_provider {
	authp_undefined,
	authp_immediate,
	authp_file,
	authp_instance
};

extern char *endpoint;
extern char *signature_version;
extern int use_ssl;
extern int use_post;
extern int ssl_verify_peer;
extern char *ssl_ca_file;
extern char *ssl_ca_path;
extern int dry_run_mode;
extern char *region_name;
extern enum authentication_provider authentication_provider;
extern char *access_file_name;
extern char *access_key;
extern char *secret_key;
extern char *security_token;
extern char *format_file;
extern int translation_enabled;
extern char *custom_map;
extern enum eclat_confirm_mode confirm_mode;

extern char *instance_store_base_url;
extern unsigned short instance_store_port;
extern char *instance_store_document_path;
extern char *instance_store_credentials_path;
extern unsigned long max_retry_sleep;
extern unsigned long max_retry_time;

typedef int (*config_finish_hook_t) (void*);

void add_config_finish_hook(config_finish_hook_t fun, void *data);

void config_help(void);
void config_init(void);
void config_finish(struct grecs_node *tree);
int run_config_finish_hooks(void);

struct eclat_command_env {
	struct eclat_command const *cmd;
	struct ec2_request *request;
	struct grecs_node *xmltree;
};

typedef struct eclat_command_env eclat_command_env_t;

typedef int (*eclat_command_handler_t) (eclat_command_env_t *env, int argc, char **argv);

struct eclat_command {
	const char *name;
	const char *ident;
	const char *tag;
	eclat_command_handler_t handler;
	int flags;
	enum eclat_confirm_mode confirm;
	char *fmt;
	struct grecs_locus locus;
};

struct eclat_command *find_command_name(const char *name);
int eclat_do_command(eclat_command_env_t *env, struct eclat_command *command,
		     int argc, char **argv);

struct eclat_io {
	XML_Parser parser;
	eclat_partial_tree_t part;
	CURL *curl;
};

struct eclat_io *eclat_io_init(int errfatal);
void eclat_io_free(struct eclat_io *io);
void eclat_io_shutdown(struct eclat_io *io);
struct grecs_node *eclat_io_finish(struct eclat_io *io);

int eclat_trace_fun(CURL *handle, curl_infotype type,
		    char *data, size_t size,
		    void *userp);


int eclat_start_instance(eclat_command_env_t *env, int argc, char **argv);
int eclat_stop_instance(eclat_command_env_t *env, int argc, char **argv);
int eclat_reboot_instance(eclat_command_env_t *env, int argc, char **argv);
int eclat_terminate_instances(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_tags(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_instance_status(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_instances(eclat_command_env_t *env, int argc, char **argv);
int eclat_allocate_address(eclat_command_env_t *env, int argc, char **argv);
int eclat_release_address(eclat_command_env_t *env, int argc, char **argv);
int eclat_associate_address(eclat_command_env_t *env, int argc, char **argv);
int eclat_disassociate_address(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_addresses(eclat_command_env_t *env, int argc, char **argv);
int eclat_move_address(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_volumes(eclat_command_env_t *env, int argc, char **argv);
int eclat_get_console_output(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_instance_attribute(eclat_command_env_t *env, int argc, char **argv);
int eclat_create_tags(eclat_command_env_t *env, int argc, char **argv);
int eclat_delete_tags(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_security_groups(eclat_command_env_t *env, int argc, char **argv);
int eclat_create_snapshot(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_snapshots(eclat_command_env_t *env, int argc, char **argv);
int eclat_delete_snapshot(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_snapshot_attribute(eclat_command_env_t *env,
				      int argc, char **argv);
int eclat_modify_snapshot_attribute(eclat_command_env_t *env,
				    int argc, char **argv);
int eclat_reset_snapshot_attribute(eclat_command_env_t *env,
				   int argc, char **argv);

int eclat_describe_avaialbility_zones(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_regions(eclat_command_env_t *env, int argc, char **argv);
int eclat_create_volume(eclat_command_env_t *env, int argc, char **argv);
int eclat_delete_volume(eclat_command_env_t *env, int argc, char **argv);
int eclat_attach_volume(eclat_command_env_t *env, int argc, char **argv);
int eclat_detach_volume(eclat_command_env_t *env, int argc, char **argv);
int eclat_modify_volume(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_volumes_modifications(eclat_command_env_t *env, int argc,
					 char **argv);

int eclat_modify_instance_attribute(eclat_command_env_t *env,
				    int argc, char **argv);
int eclat_run_instances(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_images(eclat_command_env_t *env, int argc, char **argv);
int eclat_create_image(eclat_command_env_t *env, int argc, char **argv);
int eclat_deregister_image(eclat_command_env_t *env, int argc, char **argv);
int eclat_copy_image(eclat_command_env_t *env, int argc, char **argv);
int eclat_copy_snapshot(eclat_command_env_t *env, int argc, char **argv);
int eclat_sg(eclat_command_env_t *env, int argc, char **argv);

int eclat_describe_vpcs(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_vpc_attribute(eclat_command_env_t *env, int argc, char **argv);
int eclat_modify_vpc_attribute(eclat_command_env_t *env, int argc, char **argv);
int eclat_delete_vpc(eclat_command_env_t *env, int argc, char **argv);

int eclat_create_internet_gateway(eclat_command_env_t *env,
				  int argc, char **argv);
int eclat_delete_internet_gateway(eclat_command_env_t *env,
				  int argc, char **argv);
int eclat_describe_internet_gateways(eclat_command_env_t *env,
				     int argc, char **argv);
int eclat_attach_internet_gateway(eclat_command_env_t *env,
				  int argc, char **argv);
int eclat_detach_internet_gateway(eclat_command_env_t *env,
				  int argc, char **argv);

int eclat_create_subnet(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_subnets(eclat_command_env_t *env,
			   int argc, char **argv);
int eclat_modify_subnet_attribute(eclat_command_env_t *env,
				  int argc, char **argv);
int eclat_delete_subnet(eclat_command_env_t *env, int argc, char **argv);

int eclat_create_route_table(eclat_command_env_t *env, int argc, char **argv);
int eclat_delete_route_table(eclat_command_env_t *env, int argc, char **argv);
int eclat_describe_route_tables(eclat_command_env_t *env,
				int argc, char **argv);
int eclat_associate_route_table(eclat_command_env_t *env,
				int argc, char **argv);
int eclat_disassociate_route_table(eclat_command_env_t *env,
				   int argc, char **argv);
int eclat_route(eclat_command_env_t *env, int argc, char **argv);

int eclat_create_security_group(eclat_command_env_t *env,
				int argc, char **argv);
int eclat_delete_security_group(eclat_command_env_t *env,
				int argc, char **argv);

int eclat_describe_image_attribute(eclat_command_env_t *env,
				   int argc, char **argv);
int eclat_modify_image_attribute(eclat_command_env_t *env,
				 int argc, char **argv);

int eclat_lsattr(eclat_command_env_t *env, int argc, char **argv);
int eclat_create_vpc(eclat_command_env_t *env, int argc, char **argv);

char *region_to_endpoint(const char *region);

void define_format(const char *name, const char *format, grecs_locus_t *locus);
void set_command_format(const char *name, const char *format,
			grecs_locus_t *locus);

void describe_request_create(eclat_command_env_t *env, int argc, char **argv,
			   const char *uparm);
void describe_request_update(eclat_command_env_t *env, int argc, char **argv,    
			   const char *uparm, int n_in, int *n_out);

int eclat_send_request(struct ec2_request *q, struct grecs_node **ret);
char *eclat_get_instance_zone(void);
void eclat_get_instance_creds(char *id,
			      char **access_key_ptr, char **secret_key_ptr,
			      char **token_ptr);

int eclat_actcmp(const char *a, const char *b);

#define XML_DUMP_FILE_NAME "eclat.dump.xml"

#define FILTER_STRING 0
#define FILTER_DATE   1   
#define FILTER_BOOL   2
#define FILTER_INT    3
#define FILTER_ENUM   4

struct filter_descr {
	char *name;
	int type;
	char **avail;
};

extern struct filter_descr *available_filters;

void list_filters(FILE *fp);

extern char **available_attrs;
void list_attrs(FILE *fp);
char *canonattrname(char **attrs, const char *arg, char *delim,
		    size_t *plen);
char *read_file(const char *file);

int get_scr_cols(void);

#define MAP_IMAGE       "ImageId"
#define MAP_INSTANCE    "InstanceId"
#define MAP_GROUPID     "GroupId"
#define MAP_GROUPNAME   "GroupName"
#define MAP_SNAPSHOT    "SnapshotId"
#define MAP_VOLUME      "VolumeId"
#define MAP_AZ          "AZ"
#define MAP_REG         "reg"
#define MAP_VPC         "VpcId"
#define MAP_IGW         "InternetGatewayId"
#define MAP_SUBNET      "SubnetId"
#define MAP_ROUTE_TABLE "RouteTableId"

void translate_ids(int argc, char **argv, const char *map);
void translate_resource_ids(int argc, char **argv);
void eclat_encode_devmap(struct ec2_request *q, struct grecs_list *list);

int get_access_creds(const char *id, char **access_key_ptr,
		     char **secret_key_ptr);

void define_format(const char *name, const char *fmt, grecs_locus_t *loc);
forlan_eval_env_t find_format(const char *name);

void generic_parse_options(struct eclat_command const *command,
			   const char *docstring,
			   int argc, char *argv[], int *index);
extern struct grecs_proginfo *generic_proginfo;

void set_command_confirmation(const char *name, enum eclat_confirm_mode cfmode,
			      grecs_locus_t *locus);

extern struct eclat_map_drv eclat_map_drv_ec2;

