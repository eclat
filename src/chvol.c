/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

// eclat modvol VOLID [iops=V] [size=S] [type=V]

char const *params[] = {
	"iops",
	"Iops",
	"size",
	"Size",
	"type",
	"VolumeType"
};

static char const *
getparam(char const *p, size_t len)
{
	int i;

	for (i = 0; i < sizeof(params) / sizeof(params[0]); i++) {
		if (len == strlen(params[i])
		    && memcmp(params[i], p, len) == 0) {
			if (i % 2 == 0)
				i++;
			return params[i];
		}
	}
	return NULL;
}

int
eclat_modify_volume(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
        struct ec2_request *q = env->request;

        generic_proginfo->args_doc = "VOL-ID [iops=V] [size=S] [type=V]";
        generic_parse_options(env->cmd,
                              "modify volume parameters",
                              argc, argv, &i);
        argv += i;
        argc -= i;

	if (argc == 0)
                die(EX_USAGE, "wrong number of arguments");

	translate_ids(1, argv, MAP_VOLUME);
	eclat_request_add_param(q, "VolumeId", argv[0]);
	for (i = 1; i < argc; i++) {
		size_t len;
		char const *param;
		char const *value;
		
		len = strcspn(argv[i], "=");
		param = getparam(argv[i], len);
		if (!param)
			die(EX_USAGE, "unrecognized parameter: %s", argv[i]);
		if (argv[i][len]) {
			value = argv[i] + len + 1;
		} else {
			i++;
			if (i == argc)
				die(EX_USAGE, "missing value for %s",
				    argv[i-1]);
			value = argv[i];
		} 
		eclat_request_add_param(q, param, value);
	}
	return 0;
}
		
