/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static char *attrs[] = {
	"createVolumePermission",
	"productCodes",
	NULL
};

int
eclat_describe_snapshot_attribute(eclat_command_env_t *env,
				  int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	const char *attrname;
	
	generic_proginfo->args_doc = "SNAP-ID [ATTR]";
	available_attrs = attrs;
	generic_proginfo->print_help_hook = list_attrs;
	generic_parse_options(env->cmd,
			      "describe the attribute of a snapshot",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	switch (argc) {
	default:
		die(EX_USAGE, "wrong number of arguments");
	case 2:
		attrname = canonattrname(attrs, argv[1], NULL, NULL);
		if (!attrname)
			die(EX_USAGE, "unrecognized attribute name");
		break;
	case 1:
		attrname = attrs[0];
	}
		
	translate_ids(1, argv, MAP_SNAPSHOT);
	eclat_request_add_param(q, "SnapshotId", argv[0]);
	eclat_request_add_param(q, "Attribute", attrname);
	return 0;
}

static void
remdash(char *p)
{
	char *q;

	q = p + strspn(p, "0123456789-");
	if (*q)
		die(EX_USAGE, "invalid account ID: %s; stopped near %s",
		    p, q);
	for (q = p;; p++) {
		if (*p == '-')
			continue;
		if (!(*q++ = *p))
			break;
	}
}

int
eclat_modify_snapshot_attribute(eclat_command_env_t *env,
				int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	char *bufptr = NULL;
	size_t bufsize = 0;
	
	generic_proginfo->args_doc = "{g|u}{-|+}{all|ID} SNAP-ID";
	generic_parse_options(env->cmd,
			      "modify the createVolumePermission attribute of a snapshot",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc < 2)
		die(EX_USAGE, "wrong number of arguments");
	for (i = 0; i < argc - 1; i++) {
		const char *action, *what;

		switch (argv[i][0]) {
		case 'u':
			what = "UserId";
			break;
		case 'g':
			what = "Group";
			break;
		default:
			die(EX_USAGE, "malformed argument: %s", argv[i]);
		}

		switch (argv[i][1]) {
		case '+':
			action = "Add";
			break;
		case '-':
			action = "Remove";
			break;
		default:
			die(EX_USAGE, "malformed argument: %s", argv[i]);
		}
		
		grecs_asprintf(&bufptr, &bufsize,
			       "CreateVolumePermission.%s.%d.%s", action,
			       i + 1, what);

		if (argv[i][0] == 'u')
			remdash(argv[i] + 2);
		eclat_request_add_param(q, bufptr, argv[i] + 2);
	}
	free(bufptr);
	
	translate_ids(1, argv + i, MAP_SNAPSHOT);
	eclat_request_add_param(q, "SnapshotId", argv[i]);
	
	return 0;
}

int
eclat_reset_snapshot_attribute(eclat_command_env_t *env,
			       int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	
	generic_proginfo->args_doc = "SNAP-ID";
	generic_parse_options(env->cmd,
			      "reset the createVolumePermission attribute of a snapshot",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");
	
	translate_ids(1, argv, MAP_SNAPSHOT);
	eclat_request_add_param(q, "SnapshotId", argv[0]);
	eclat_request_add_param(q, "Attribute", "CreateVolumePermission");

	return 0;
}
	
