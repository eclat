/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
char *zone;
char *snapshot;
char *iops;
#include "mkvol-cl.h"

int
eclat_create_volume(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	
	parse_options(env, argc, argv, &i);
	argc -= i;
	argv += i;
	if (argc != 2)
		die(EX_USAGE, "wrong number of arguments");

	eclat_request_add_param(q, "Size", argv[0]);
	eclat_request_add_param(q, "AvailabilityZone", argv[1]);
	if (snapshot)
		eclat_request_add_param(q, "SnapshotId", snapshot);
	if (iops) {
		eclat_request_add_param(q, "VolumeType", "io1");
		eclat_request_add_param(q, "Iops", iops);
	}
	return 0;
}

		

