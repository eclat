/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
#include "mktags-cl.h"

static int
process_tags(eclat_command_env_t *env, int argc, char **argv, int require_tags)
{
	int i, j;
	struct ec2_request *q = env->request;
	char *bufptr = NULL;
	size_t bufsize = 0;
	struct grecs_list_entry *ep;
	
	if (!require_tags)
		proginfo.args_doc = "[TAG[=VALUE...]]";
	parse_options(env, argc, argv, &i);
	argv += i;
	argc -= i;

	if (!reslist->head)
		die(EX_USAGE, "no resource IDs supplied");
	if (require_tags && argc == 0 && !from_file)
		die(EX_USAGE, "no tags supplied");

	for (ep = reslist->head, i = 1; ep; ep = ep->next, i++) {
		struct resource *res = ep->data;
		grecs_asprintf(&bufptr, &bufsize, "ResourceId.%d", i);
		if (res->map)
			translate_ids(1, &res->resid, res->map);
		eclat_request_add_param(q, bufptr, res->resid);
	}

	j = 1;
	if (from_file) {
		FILE *fp;
		char *inbuf = NULL;
		size_t insize = 0;
		ssize_t rc;
		char *p;
		struct wordsplit ws;
		int wsflags;
		
		if (strcmp(from_file, "-") == 0) {
			from_file = "<stdin>";
			fp = stdin;
		} else {
			fp = fopen(from_file, "r");
			if (!fp)
				die(EX_NOINPUT, "cannot open file \"%s\": %s",
				    from_file, strerror(errno));
		}

		wsflags = WRDSF_DEFFLAGS|WRDSF_NOSPLIT;
		while ((rc = grecs_getline(&inbuf, &insize, fp)) > 0) {
			char *text;
			if (wordsplit(inbuf, &ws, wsflags)) {
				die(EX_SOFTWARE, "%s", wordsplit_strerror(&ws));
			}
			wsflags |= WRDSF_REUSE;
			text = ws.ws_wordv[0];
			p = strchr(text, '=');
			if (p)
				*p++ = 0;
			grecs_asprintf(&bufptr, &bufsize, "Tag.%d.Key", j);
			eclat_request_add_param(q, bufptr, text);
			if (p) {
				grecs_asprintf(&bufptr, &bufsize,
					       "Tag.%d.Value", j);
				eclat_request_add_param(q, bufptr, p);
			}
			++j;
		}
		if (wsflags & WRDSF_REUSE)
			wordsplit_free(&ws);
		
		fclose(fp);
		free(inbuf);
	}
	
	for (i = 0; i < argc; i++, j++) {
		char *p = strchr(argv[i], '=');
		if (p)
			*p++ = 0;
		grecs_asprintf(&bufptr, &bufsize, "Tag.%d.Key", j);
		eclat_request_add_param(q, bufptr, argv[i]);
		if (p) {
			grecs_asprintf(&bufptr, &bufsize, "Tag.%d.Value", j);
			eclat_request_add_param(q, bufptr, p);
		}
	}
	free(bufptr);

	return 0;
}
	
int
eclat_create_tags(eclat_command_env_t *env, int argc, char **argv)
{
	proginfo.progname = "eclat create-tags";
	proginfo.docstring = "create tags for the specified resources";
	return process_tags(env, argc, argv, 1);
}

int
eclat_delete_tags(eclat_command_env_t *env, int argc, char **argv)
{
	proginfo.progname = "eclat delete-tags";
	proginfo.docstring = "delete tags from the specified resources";
	return process_tags(env, argc, argv, 0);
}
