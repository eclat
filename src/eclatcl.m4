/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

define([<ECLAT_CL_BEGIN>],[<
OPTIONS_COMMAND_BEGIN("eclat",
              [<>],
              [<$1>],
	      [<$2>],
	      [<gnu>],
	      [<noversion>])>])

define([<ECLAT_CL_END>],[<OPTIONS_END>])

define([<ECLAT_CL_PARSER>],[<
static void
$1(eclat_command_env_t *env, $2)
{
	const char *cmds[3] = { env->cmd->name, env->cmd->ident, NULL };
	proginfo.subcmd = (char**) cmds;
	$3
}
>])
	
