# This file is part of Eclat 
# Copyright (C) 2012-2023 Sergey Poznyakoff
#
# Eclat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Eclat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Eclat.  If not, see <http://www.gnu.org/licenses/>.

bin_PROGRAMS=eclat ispeek

eclat_SOURCES=\
 accfile.c\
 allocaddr.c\
 asscaddr.c\
 atvol.c\
 chvol.c\
 cmdline.h\
 config.c\
 cpimg.c\
 cpsnap.c\
 deimg.c\
 devmap.c\
 devol.c\
 disasaddr.c\
 ec2map.c\
 eclat.c\
 eclat.h\
 genericcl.c\
 getconout.c\
 igw.c\
 io.c\
 lsaddr.c\
 lsattr.c\
 lsaattr.c\
 lschvol.c\
 lsiattr.c\
 lsimg.c\
 lsinst.c\
 lsistat.c\
 lsreg.c\
 lssattr.c\
 lssg.c\
 lssnap.c\
 lstag.c\
 lsvol.c\
 lsvpc.c\
 lszon.c\
 mkimg.c\
 mkinst.c\
 mksg.c\
 mksnap.c\
 mktags.c\
 mkvol.c\
 mkvpc.c\
 mvaddr.c\
 rmaddr.c\
 rmsg.c\
 rmsnap.c\
 rmvol.c\
 rmvpc.c\
 routetable.c\
 setiattr.c\
 sg.c\
 sg.h\
 setaattr.c\
 startstop.c\
 subnet.c\
 util.c\
 vpcattr.c

ispeek_SOURCES = \
 ispeek.c\
 ispeek-cl.h

LDADD=../lib/libeclat.a @LIBOBJS@ ../grecs/src/libgrecs.a $(CURL_LIBS) @MAPLIBS@
AM_CPPFLAGS = \
 @GRECS_INCLUDES@ -I$(top_srcdir)/lib $(CURL_CFLAGS)\
 -DSYSCONFDIR=\"$(sysconfdir)\"\
 -DDEFAULT_VERSION_INCLUDE_DIR=\"$(incdir)\"\
 -DDEFAULT_INCLUDE_DIR=\"$(pkgdatadir)/include\"\
 -DDEFAULT_PREPROCESSOR="$(DEFAULT_PREPROCESSOR)"

OPTFILES=\
 allocaddr-cl.opt\
 asscaddr-cl.opt\
 cpimg-cl.opt\
 cpsnap-cl.opt\
 devol-cl.opt\
 disasaddr-cl.opt\
 generic-cl.opt\
 lsaddr-cl.opt\
 lsattr-cl.opt\
 lsimg-cl.opt\
 lsistat-cl.opt\
 lssg-cl.opt\
 lssnap-cl.opt\
 lstag-cl.opt\
 mkimg-cl.opt\
 mkinst-cl.opt\
 mksg-cl.opt\
 mksubnet-cl.opt\
 mktags-cl.opt\
 mkvol-cl.opt\
 mkvpc-cl.opt\
 rmaddr-cl.opt\
 rmsg-cl.opt\
 sg-cl.opt\
 setiattr-cl.opt\
 setaattr-cl.opt\
 stop-cl.opt

eclat_SOURCES += $(OPTFILES:.opt=.h)

BUILT_SOURCES=\
 cmdline.h\
 ispeek-cl.h\
 $(OPTFILES:.opt=.h)\
 comtab.man\
 xref.man

EXTRA_DIST=\
 cmdline.opt\
 ispeek-cl.opt\
 $(OPTFILES)\
 comtab.man\
 xref.man\
 eclatcl.m4

SUFFIXES=.opt .c .h

.opt.h:
	$(AM_V_GEN)m4 -s $(top_srcdir)/@GRECS_SUBDIR@/build-aux/getopt.m4 eclatcl.m4 $< | sed '1d' > $@

comtab.man: eclat.c
	$(AM_V_GEN)if test -f eclat.c; then\
		test -x ./eclat || make eclat || exit 1; \
		./eclat -l '\t\\fB%n\\fR\t\\fB%i\\fR\n' | sed 's/-/\\-/g' > comtab.man; \
	else \
		echo >&2 "comtab.man must be built in $(top_srcdir)/src"; \
		exit 1; \
	fi

xref.man: eclat.c
	$(AM_V_GEN)if test -f eclat.c; then\
		test -x ./eclat || make eclat || exit 1; \
		./eclat -l '%.BR eclat\\-: (1)n,\n' > xref.man; \
	else \
		echo >&2 "xref.man must be built in $(top_srcdir)/src"; \
		exit 1; \
	fi

