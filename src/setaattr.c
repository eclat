/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

#define ACCTYPE_USERID 0
#define ACCTYPE_GROUP  1

char *acct_string[] = { "UserId", "Group"  };

#define ACT_ADD 0
#define ACT_DEL 1
char *action_string[] = { "Add", "Remove" };

int action = ACT_ADD;
int acctype = ACCTYPE_USERID;

#include "setaattr-cl.h"

static char *attrs[] = {
	"description",
	"LaunchPermission",
	"ProductCode",
	NULL
};

static char *
canonuserid(char *input)
{
	char *p, *q;

	for (p = q = input; *q; q++) {
		if (strchr(" \t-", *q))
			continue;
		*p++ = *q;
	}
	*p = 0;
	return input;
}

int
eclat_modify_image_attribute(eclat_command_env_t *env,
			     int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;
	const char *attrname;
	char *bufptr = NULL;
	size_t bufsize = 0;

	available_attrs = attrs;
	proginfo.print_help_hook = list_attrs;

	parse_options(env, argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc < 3)
		die(EX_USAGE, "wrong number of arguments");
	translate_ids(1, argv, MAP_IMAGE);
	attrname = canonattrname(attrs, argv[1], NULL, NULL);
	if (!attrname)
		die(EX_USAGE, "unrecognized attribute name");

	eclat_request_add_param(q, "ImageId", argv[0]);

	argv++;
	argc--;
	argv[0] = (char*) env->cmd->name;
	
	if (strcmp(attrname, "description") == 0) {
		if (argc != 2)
			die(EX_USAGE, "only one description is allowed");
		eclat_request_add_param(q, "Description.Value", argv[1]);
	} else if (strcmp(attrname, "ProductCode") == 0) {
		for (i = 1; i < argc; i++) {
			grecs_asprintf(&bufptr, &bufsize,
				       "ProductCode.%d",
				       i);
			eclat_request_add_param(q, bufptr, argv[i]);
		}
	} else {
		int j = 1;
		
		while (argc) {
			argv[0] = (char*) env->cmd->name;
			parse_options(env, argc, argv, &i);
			argv += i;
			argc -= i;

			if (argc) {
				grecs_asprintf(&bufptr, &bufsize,
					       "LaunchPermission.%s.%d.%s",
					       action_string[action], j++,
					       acct_string[acctype]);
				eclat_request_add_param(q, bufptr,
						      canonuserid(argv[0]));
			}
		}
	}
	free(bufptr);
	return 0;
}
	
	
	
