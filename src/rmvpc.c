/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

int
eclat_delete_vpc(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	struct ec2_request *q = env->request;

	generic_proginfo->args_doc = "VPC-ID";
	generic_parse_options(env->cmd,
			      "delete the VPC",
			      argc, argv, &i);
	argv += i;
	argc -= i;

	if (argc != 1)
		die(EX_USAGE, "wrong number of arguments");

	translate_ids(1, argv, MAP_VPC);
	eclat_request_add_param(q, "VpcId", argv[0]);

	return 0;
}
	
