/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

ECLAT_CL_BEGIN([<describe the status of Amazon EC2 instances including any scheduled events>],
	       [<[FILTER...]>])

OPTION(all,a,,
       [<return the health status for all instances (default)>])
BEGIN
	all_option = 1;
END

OPTION(running,r,,
       [<show only running instances>])
BEGIN
	all_option = 0;
END	

ECLAT_CL_END

ECLAT_CL_PARSER(parse_options, [<int argc, char *argv[], int *index>], [<
{
	static char *event_codes[] = {
		"instance-reboot",
		"system-reboot",
		"system-maintenance",
		"instance-retirement",
		NULL
	};
	static char *instance_state_name[] = {
		"pending",
		"running",
		"shutting-down",
		"terminated",
		"stopping",
		"stopped",
		NULL
	};
	static char *instance_state_code[] = {
		"0 (pending)",
		"16 (running)",
		"32 (shutting-down)",
		"48 (terminated)",
		"64 (stopping)",
		"80 (stopped)"
	};
	static char *system_status[] = {
		"ok",
		"impaired",
		"initializing",
		"insufficient-data",
		"not-applicable",
		NULL
	};
	static char *system_reachability[] = {
		"passed",
		"failed",
		"initializing",
		"insufficient-data",
		NULL
	};
	static char *instance_status[] = {
		"ok",
		"impaired",
		"initializing",
		"insufficient-data",
		"not-applicable"
	};
	static char *instance_reachability[] = {
		"passed",
		"failed",
		"initializing",
		"insufficient-data",
		NULL
	};
	static struct filter_descr filters[] = {
		{ "availability-zone",   FILTER_STRING },
		{ "event.code",          FILTER_ENUM, event_codes },
		{ "event.description",   FILTER_STRING },
		{ "event.not-after",     FILTER_DATE },
		{ "instance-state-name", FILTER_ENUM, instance_state_name },
		{ "instance-state-code", FILTER_ENUM, instance_state_code },
		{ "system-status.status",FILTER_ENUM, system_status },
		{ "system-status.reachability",
		                         FILTER_ENUM, system_reachability },
		{ "instance-status.status",
		                         FILTER_ENUM, instance_status },
		{ "instance-status.reachability",
		                         FILTER_ENUM, instance_reachability },
		{ NULL }
	};

	available_filters = filters;
	proginfo.print_help_hook = list_filters;
	GETOPT(argc, argv, *index, exit(EX_USAGE))
}
>])
