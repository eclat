/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static void
parse_options(struct eclat_command const *cmd, int argc, char *argv[],
	      int *index)
{
	static char *architecture_str[] = { "i386", "x86_64", NULL };
	static char *lifecycle_str[] = { "spot", NULL };
	static char * state_code_str[] = {
		"0 (pending)", "16 (running)", "32 (shutting-down)",
		"48 (terminated)", "64 (stopping)", "80 (stopped)",
		NULL
	};
	static char *state_name_str[] = {
		"pending", "running",
		"shutting-down", "terminated",
		"stopping", "stopped",
		NULL
	};
	static char *state_str[] = { "disabled", "enabled", NULL };
	static char *prod_code_str[] = { "devpay", "marketplace",
					 NULL };
	static char *device_type_str[] = {
		"ebs",
		"instance-store",
		NULL
	};
	static char *virt_type_str[] = { "paravirtual", "hvm", NULL };
	static char *hypervisor_str[] = { "ovm", "xen", NULL };
	static char *iface_status_str[] = { "available", "in-use", NULL };
	static char *iface_att_str[] = {
		"attaching", "attached", "detaching", "detached", NULL
	};
	static struct filter_descr filters[] = {
		{ "architecture",    FILTER_ENUM, architecture_str },
		{ "availability-zone", FILTER_STRING },
		{ "block-device-mapping.attach-time", FILTER_DATE },
		{ "block-device-mapping.delete-on-termination", FILTER_BOOL },
		{ "block-device-mapping.device-name", FILTER_STRING },
		{ "block-device-mapping.status", FILTER_STRING },
		{ "block-device-mapping.volume-id", FILTER_STRING },
		{ "client-token", FILTER_STRING },
		{ "dns-name", FILTER_STRING },
		{ "group-id", FILTER_STRING },
		{ "group-name", FILTER_STRING },
		{ "image-id", FILTER_STRING },
		{ "instance-id", FILTER_STRING },
		{ "instance-lifecycle", FILTER_ENUM, lifecycle_str },
		{ "instance-state-code", FILTER_ENUM, state_code_str },
		{ "instance-state-name", FILTER_ENUM, state_name_str },
		{ "instance-type", FILTER_STRING },
		{ "instance.group-id", FILTER_STRING },
		{ "instance.group-name", FILTER_STRING },
		{ "ip-address", FILTER_STRING },
		{ "kernel-id", FILTER_STRING },
		{ "key-name", FILTER_STRING },
		{ "launch-index", FILTER_STRING },
		{ "launch-time", FILTER_DATE },
		{ "monitoring-state", FILTER_ENUM, state_str },
		{ "owner-id", FILTER_STRING },
		{ "placement-group-name", FILTER_STRING },
		{ "private-dns-name", FILTER_STRING },
		{ "private-ip-address", FILTER_STRING },
		{ "product-code", FILTER_STRING },
		{ "product-code.type", FILTER_ENUM, prod_code_str },
		{ "ramdisk-id", FILTER_STRING },
		{ "reason", FILTER_STRING },
		{ "requester-id", FILTER_STRING },
		{ "reservation-id", FILTER_STRING },
		{ "root-device-name", FILTER_STRING },
		{ "root-device-type", FILTER_ENUM, device_type_str },
		{ "source-dest-check", FILTER_BOOL },
		{ "spot-instance-request-id", FILTER_STRING },
		{ "state-reason-code", FILTER_STRING },
		{ "state-reason-message", FILTER_STRING },
		{ "subnet-id", FILTER_STRING },
		{ "tag-key", FILTER_STRING },
		{ "tag-value", FILTER_STRING },
		{ "tag:<KEY>", FILTER_STRING },
		{ "virtualization-type", FILTER_STRING, virt_type_str },
		{ "vpc-id", FILTER_STRING },
		{ "hypervisor", FILTER_ENUM, hypervisor_str },
		{ "network-interface.description", FILTER_STRING },
		{ "network-interface.subnet-id", FILTER_STRING },
		{ "network-interface.vpc-id", FILTER_STRING },
		{ "network-interface.network-interface.id", FILTER_STRING },
		{ "network-interface.owner-id", FILTER_STRING },
		{ "network-interface.availability-zone", FILTER_STRING },
		{ "network-interface.requester-id", FILTER_STRING },
		{ "network-interface.requester-managed", FILTER_BOOL },
		{ "network-interface.status", FILTER_ENUM, iface_status_str },
		{ "network-interface.mac-address", FILTER_STRING },
		{ "network-interface-private-dns-name", FILTER_STRING },
		{ "network-interface.source-destination-check", FILTER_BOOL },
		{ "network-interface.group-id", FILTER_STRING },
		{ "network-interface.group-name", FILTER_STRING },
		{ "network-interface.attachment.attachment-id", FILTER_STRING },
		{ "network-interface.attachment.instance-id", FILTER_STRING },
		{ "network-interface.attachment.instance-owner-id", FILTER_STRING },
		{ "network-interface.addresses.private-ip-address", FILTER_STRING },
		{ "network-interface.attachment.device-index", FILTER_INT },
		{ "network-interface.attachment.status", FILTER_ENUM,
		  iface_att_str },
		{ "network-interface.attachment.attach-time", FILTER_DATE },
		{ "network-interface.attachment.delete-on-termination",
		  FILTER_BOOL },
		{ "network-interface.addresses.primary", FILTER_BOOL },
		{ "network-interface.addresses.association.public-ip",
		  FILTER_STRING },
		{ "network-interface.addresses.association.ip-owner-id", FILTER_STRING },
		{ "association.public-ip", FILTER_STRING },
		{ "association.ip-owner-id", FILTER_STRING },
		{ "association.allocation-id", FILTER_STRING },
		{ NULL }
	};

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = _("[FILTER...] [INST-ID...]");
	return generic_parse_options(cmd,
				     "List instances",
				     argc, argv, index);
}

int
eclat_describe_instances(eclat_command_env_t *env, int argc, char **argv)
{
	int i;
	
	parse_options(env->cmd, argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_INSTANCE);

	describe_request_create(env, argc, argv, "InstanceId");
	return 0;
}
