/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_GETOPT_H
# include <getopt.h>
#endif

OPTIONS_BEGIN("ispeek",
              [<EC2 instance store lister>],
	      [<PATH [KEY...]>],
	      [<gnu>],
              [<copyright_year=2012-2018>],
              [<copyright_holder=Sergey Poznyakoff>])

OPTION(base,b,URL,
       [<base URL>])
BEGIN
        base_url = optarg;
END
	      
OPTION(port,p,NUMBER,
       [<set remote port number>])
BEGIN
	char *p;
	
	errno = 0;
	port = strtol(optarg, &p, 10);
	if (port <= 0 || port > USHRT_MAX || errno || *p)
		die(EX_USAGE, "invalid port number");
END	
	
OPTION(recursive,r,,
       [<list recursively>])
BEGIN
	recursive = 1;
END

OPTION(names,N,,
       [<print key names>])
BEGIN
	print_options |= PRINT_NAME;
END	

OPTION(quote,Q,,
       [<quote strings>])
BEGIN
	print_options |= PRINT_QUOTE;
END

OPTION(type,T,,
       [<print value types>])
BEGIN
	print_options |= PRINT_TYPE;
END       

OPTION(delimiter,D,STRING,
       [<use STRING as a delimiter, instead of colon>])
BEGIN
	delim = optarg;
END	

OPTION(debug,d,,
       [<increase debugging level>])
BEGIN
	debug_category[0].level++;
END

OPTIONS_END

static void
parse_options(int *pargc, char **pargv[])
{
	int argc = *pargc;
	char **argv = *pargv;
	int index;
	
        GETOPT(argc, argv, index)
	*pargc -= index;
	*pargv += index;
}

