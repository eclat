/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
static int vpc;
static char *iface;
static char *privaddr;
static int reassoc;
#include "asscaddr-cl.h"

int
eclat_associate_address(eclat_command_env_t *env, int argc, char **argv)
{
	int i;

	parse_options(env, argc, argv, &i);
	argc -= i;
	argv += i;

	if (argc != 2)
		die(EX_USAGE, "wrong number of arguments to associate-address");

	translate_ids(1, argv, MAP_INSTANCE);
	eclat_request_add_param(env->request, "InstanceId", argv[0]);
	if (vpc) {
		eclat_request_add_param(env->request, "AllocationId", argv[1]);
		if (iface)
			eclat_request_add_param(env->request, "NetworkInterfaceId",
                                              iface);
		if (privaddr)
			eclat_request_add_param(env->request, "PrivateIpAddress", 
                                              privaddr);
		if (reassoc)
			eclat_request_add_param(env->request, "AllowReassociation", 
                                              "true");
		
	} else {
		eclat_request_add_param(env->request, "PublicIp", argv[1]);
	}
	return 0;
}
