/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"

static void
parse_options(struct eclat_command const *cmd,
	      int argc, char *argv[], int *index)
{
	static char *states[] = {
		"available", NULL
	};
	
	static struct filter_descr filters[] = {
		{ "message",       FILTER_STRING },
		{ "region-name",   FILTER_STRING },
		{ "state", FILTER_ENUM, states },
		{ "zone-name",     FILTER_STRING },
		{ NULL }
	};

	available_filters = filters;
	generic_proginfo->print_help_hook = list_filters;
	generic_proginfo->args_doc = "[ZONE [ZONE...]]";
	return generic_parse_options(cmd,
				     "List availability zones",
				     argc, argv, index);
}

int
eclat_describe_avaialbility_zones(eclat_command_env_t *env,
				  int argc, char **argv)
{
	int i;
	
	parse_options(env->cmd, argc, argv, &i);
	argv += i;
	argc -= i;
	translate_ids(argc, argv, MAP_AZ);

	describe_request_create(env, argc, argv, "ZoneName");
	return 0;
}
