/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "eclat.h"
#include <glob.h>

char *access_file_name;

static void
skipline(FILE *fp)
{
	int c;

	while ((c = getc(fp)) != EOF && c != '\n')
		;
}

static int
access_file_lookup(const char *filename,
		   const char *id, char **access_key_ptr, char **secret_key_ptr)
{
	int rc;
	int c;
	FILE *fp;
	size_t line = 0;
	struct grecs_txtacc *acc;
	const char *p;

	debug(ECLAT_DEBCAT_MAIN, 1, ("trying file %s", filename));
	fp = fopen(filename, "r");
	if (!fp) {
		if (errno == EACCES)
			debug(ECLAT_DEBCAT_MAIN, 1,
			      ("cannot open \"%s\": %s",
			       filename, strerror(errno)));
		else
			err("cannot open \"%s\": %s",
			    filename, strerror(errno));
		return 1;
	}
	
	while ((c = getc(fp)) != EOF) {
		line++;
		while (c != EOF && (c == ' ' || c == '\t'))
			c = getc(fp);
		if (c == '\n')
			continue;
		if (c == '#') {
			if (id) {				
				c = getc(fp);
				if (c == ':') {
					p = id;
					while (*p && (c = getc(fp)) == *p)
						p++;
					if (*p == 0 &&
					    ((c = getc(fp)) == ' ' ||
					     c == '\t' ||
					     c == '\n')) {
						id = NULL;
						if (c != '\n')
							skipline(fp);
						continue;
					}
				} else
					ungetc(c, fp);
			}
			skipline(fp);
			continue;
		}
		if (c == EOF)
			break;
		if (id) {
			for (p = id; c != EOF && c != ':' && *p == c; p++)
				c = getc(fp);
			if (c == EOF)
				break;
			if (c == '\n')
				continue;
			if (c != ':') {
				skipline(fp);
				continue;
			}
		} else {
			acc = grecs_txtacc_create();
			while (c != EOF && c != ':') {
				if (c == '\n') {
					err("%s:%u: incomplete line",
					    filename, line);
					break;
				}
				grecs_txtacc_grow_char(acc, c);
				c = getc(fp);
			}
			if (c == ':') {
				grecs_txtacc_grow_char(acc, 0);
				*access_key_ptr = grecs_txtacc_finish(acc, 1);
			}
			grecs_txtacc_free(acc);
		}
		break;
	}

	if (c == ':') {
		debug(ECLAT_DEBCAT_MAIN, 1, ("%s:%u: found credentials",
					     filename, line));
		acc = grecs_txtacc_create();
		while ((c = getc(fp)) != EOF && c != '\n')
			grecs_txtacc_grow_char(acc, c);
		grecs_txtacc_grow_char(acc, 0);
		*secret_key_ptr = grecs_txtacc_finish(acc, 1);
		grecs_txtacc_free(acc);
		rc = 0;
	} else
		rc = 1;

	fclose(fp);

	return rc;
}

static int
globerrfunc(const char *epath, int eerrno)
{
        err("%s: %s", epath, strerror(eerrno));
        return 1;
}

int
get_access_creds(const char *id, char **access_key_ptr, char **secret_key_ptr)
{
	int i;
	glob_t g;
	int rc = 1;

	if (!access_file_name) {
		debug(ECLAT_DEBCAT_MAIN, 1, ("no access file given"));
		return 1;
	}
	debug(ECLAT_DEBCAT_MAIN, 1, ("Looking for authentication credentials"));
	switch (glob(access_file_name, 0, globerrfunc, &g)) {
	case 0:
		break;
	case GLOB_NOSPACE:
		grecs_alloc_die();
	case GLOB_NOMATCH:
		debug(ECLAT_DEBCAT_MAIN, 1, 
		      ("No file matching %s", access_file_name));
		return 1;
	default:
		err("globbing error");
		return 1;
	}

	for (i = 0; rc != 0 && i < g.gl_pathc; i++)
		rc = access_file_lookup(g.gl_pathv[i], id,
					access_key_ptr, secret_key_ptr);
	globfree(&g);

	return rc;
}
