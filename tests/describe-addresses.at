# This file is part of Eclat -*- Autotest -*-
# Copyright (C) 2012-2023 Sergey Poznyakoff
#
# Eclat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Eclat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Eclat.  If not, see <http://www.gnu.org/licenses/>.

ECLAT_TEST_FORMAT([DescribeAddresses (standard)],
[DescribeAddresses, DescribeAddresses-standard],
[describe-addresses.fln],
[<DescribeAddressesResponse xmlns="http://ec2.amazonaws.com/doc/2012-08-15/">
   <requestId>59dbff89-35bd-4eac-99ed-be587EXAMPLE</requestId> 
   <addressesSet> 
      <item>
         <publicIp>192.0.2.1</publicIp>
         <domain>standard</domain>
         <instanceId>i-f15ebb98</instanceId>
      </item>
      <item>
         <publicIp>198.51.100.2</publicIp>
         <domain>standard</domain>
         <instanceId/>
      </item> 
   </addressesSet> 
</DescribeAddressesResponse>
],
[standard	192.0.2.1	i-f15ebb98
standard	198.51.100.2	
])

ECLAT_TEST_FORMAT([DescribeAddresses (vpc)],
[DescribeAddresses, DescribeAddresses-vpc],
[describe-addresses.fln],
[<DescribeAddressesResponse xmlns="http://ec2.amazonaws.com/doc/2012-08-15/">
   <requestId>0782c68a-5f24-4dce-93c0-b5a066d6e0d0</requestId>
   <addressesSet>
     <item>
       <publicIp>203.0.113.12</publicIp>
       <allocationId>eipalloc-08229861</allocationId>
       <domain>vpc</domain>
       <instanceId>i-64600030</instanceId>
       <associationId>eipassoc-f0229899</associationId>
       <networkInterfaceId>eni-ef229886</networkInterfaceId>
       <networkInterfaceOwnerId>053230519467</networkInterfaceOwnerId>
       <privateIpAddress>10.0.0.228</privateIpAddress>
     </item>
     <item>
       <publicIp>46.51.221.164</publicIp>
       <allocationId>eipalloc-1b5fe072</allocationId>
       <domain>vpc</domain>
     </item>
     <item>
       <publicIp>203.0.113.14</publicIp>
       <allocationId>eipalloc-f38a359a</allocationId>
       <domain>vpc</domain>
       <instanceId>i-7a00642e</instanceId>
       <associationId>eipassoc-1f239876</associationId>
       <networkInterfaceId>eni-d83388b1</networkInterfaceId>
       <networkInterfaceOwnerId>053230519467</networkInterfaceOwnerId>
       <privateIpAddress>10.0.0.12</privateIpAddress>
     </item>
     <item>
       <publicIp>203.0.113.33</publicIp>
       <allocationId>eipalloc-282d9641</allocationId>
       <domain>vpc</domain>
       <instanceId>i-7a00642e</instanceId>
       <associationId>eipassoc-252d964c</associationId>
       <networkInterfaceId>eni-d83388b1</networkInterfaceId>
       <networkInterfaceOwnerId>053230519467</networkInterfaceOwnerId>
       <privateIpAddress>10.0.0.14</privateIpAddress>
     </item>
     <item>
       <publicIp>203.0.113.22</publicIp>
       <allocationId>eipalloc-1266dd7b</allocationId>
       <domain>vpc</domain>
       <instanceId>i-880f6fdc</instanceId>
       <associationId>eipassoc-832e94ea</associationId>
       <networkInterfaceId>eni-af2e94c6</networkInterfaceId>
       <networkInterfaceOwnerId>053230519467</networkInterfaceOwnerId>
       <privateIpAddress>10.0.0.47</privateIpAddress>
     </item>
     <item>
       <publicIp>203.0.113.42</publicIp>
       <allocationId>eipalloc-ff229896</allocationId>
       <domain>vpc</domain>
     </item>
     <item>
       <publicIp>203.0.113.53</publicIp>
       <allocationId>eipalloc-b463dcdd</allocationId>
       <domain>vpc</domain>
       <instanceId>i-c844219c</instanceId>
       <associationId>eipassoc-d667ddbf</associationId>
       <networkInterfaceId>eni-ea67dc83</networkInterfaceId>
       <networkInterfaceOwnerId>053230519467</networkInterfaceOwnerId>
       <privateIpAddress>10.0.0.174</privateIpAddress>
     </item>
     <item>
       <publicIp>203.0.113.61</publicIp>
       <allocationId>eipalloc-bf66dcd6</allocationId>
       <domain>vpc</domain>
       <instanceId>i-ba6a0dee</instanceId>
       <associationId>eipassoc-9c66dcf5</associationId>
       <networkInterfaceId>eni-73e05a1a</networkInterfaceId>
       <networkInterfaceOwnerId>053230519467</networkInterfaceOwnerId>
       <privateIpAddress>10.0.0.85</privateIpAddress>
     </item>
   </addressesSet>
</DescribeAddressesResponse>
],
[vpc	203.0.113.12	i-64600030	eni-ef229886	053230519467	10.0.0.228	eipalloc-08229861	eipassoc-f0229899
vpc	46.51.221.164					eipalloc-1b5fe072	
vpc	203.0.113.14	i-7a00642e	eni-d83388b1	053230519467	10.0.0.12	eipalloc-f38a359a	eipassoc-1f239876
vpc	203.0.113.33	i-7a00642e	eni-d83388b1	053230519467	10.0.0.14	eipalloc-282d9641	eipassoc-252d964c
vpc	203.0.113.22	i-880f6fdc	eni-af2e94c6	053230519467	10.0.0.47	eipalloc-1266dd7b	eipassoc-832e94ea
vpc	203.0.113.42					eipalloc-ff229896	
vpc	203.0.113.53	i-c844219c	eni-ea67dc83	053230519467	10.0.0.174	eipalloc-b463dcdd	eipassoc-d667ddbf
vpc	203.0.113.61	i-ba6a0dee	eni-73e05a1a	053230519467	10.0.0.85	eipalloc-bf66dcd6	eipassoc-9c66dcf5
])
