/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libeclat.h"

int
main(int argc, char **argv)
{
	XML_Parser parser;
	eclat_partial_tree_t part;
	FILE *fp;
	char buffer[128];
	size_t size;
	const char *filename;
	struct grecs_node *tree;
	
	if (argc > 2) {
		fprintf(stderr, "usage: %s [string]\n", argv[0]);
		return 1;
	}

	if (argc == 1 || strcmp(argv[1], "-") == 0) {
		fp = stdin;
		filename = "<stdin>";
	} else {
		filename = argv[1];
		fp = fopen(filename, "r");
		if (!fp) {
			perror(filename);
			return 1;
		}
	}

	parser = XML_ParserCreate("UTF-8");
	if (!parser) {
		fprintf(stderr, "cannot create XML parser\n");
		return 1;
	}

	XML_SetElementHandler(parser,
			      eclat_partial_tree_start_handler,
			      eclat_partial_tree_end_handler);
	XML_SetCharacterDataHandler(parser,
				    eclat_partial_tree_data_handler);
	part = eclat_partial_tree_create();
	XML_SetUserData(parser, part);

	while ((size = fread(buffer, 1, sizeof(buffer), fp)) > 0) {
		enum XML_Status status;

		status = XML_Parse(parser, buffer, size, 0);
		if (status == XML_STATUS_ERROR) {
			enum XML_Error error = XML_GetErrorCode(parser);
			int line = XML_GetCurrentLineNumber(parser);
			int column = XML_GetCurrentColumnNumber(parser);

			fprintf(stderr, "%s:%d:%d: %s\n",
				filename, line, column, XML_ErrorString(error));
			return 1;
		}
	}
	XML_Parse(parser, "", 0, 1);
	
	tree = eclat_partial_tree_finish(part);

	grecs_print_node(tree, GRECS_NODE_FLAG_DEFAULT, stdout);
	fputc('\n', stdout);
	grecs_tree_free(tree);

	return 0;
}
