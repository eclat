/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libeclat.h"

int
main(int argc, char **argv)
{
	char *output;
	size_t outlen;
	
	if (argc != 2) {
		fprintf(stderr, "usage: %s string\n", argv[0]);
		return 1;
	}
	
	urlencode(argv[1], strlen(argv[1]), &output, &outlen);

	printf("%lu\n", (unsigned long) outlen);
	printf("%s\n", output);
	return 0;
}

	
		

	
	
	
