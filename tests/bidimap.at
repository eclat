# This file is part of Eclat -*- Autotest -*-
# Copyright (C) 2012-2023 Sergey Poznyakoff
#
# Eclat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Eclat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Eclat.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([bidi])
AT_KEYWORDS([map bidimap])

AT_DATA([test.conf],
[map dir {
	type file;
	file "dir.txt";
}

map rev {
	type file;
	file "rev.txt";
};

map inst {
	type bidi;
	direct-map dir;
	reverse-map rev;
}
])

AT_DATA([dir.txt],
[db:i-feed1234
web:i-deadbeef
])

AT_DATA([rev.txt],
[i-deadbeef:web
i-feed1234:db
])

AT_CHECK([eclat --config-file test.conf --test-map inst:0 db],
[0],
[i-feed1234
])

AT_CHECK([eclat --config-file test.conf --test-map inst:1 i-feed1234],
[0],
[db
])

AT_CLEANUP
