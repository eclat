/* Default configuration file for Eclat.
 *
 * See eclat.conf(5) or run "info eclat configuration" for a detailed
 * description.
 */

# Use this endpoint by default.
default-endpoint "ec2.amazonaws.com";

# Declare what endpoints to use for each availability region.
# The syntax is:
#    region REGNAME ENDPOINT;

# US East (Northern Virginia) Region
region	us-east-1	ec2.us-east-1.amazonaws.com;
# US West (Oregon) Region
region	us-west-2	ec2.us-west-2.amazonaws.com;
# US West (Northern California) Region
region	us-west-1	ec2.us-west-1.amazonaws.com;
# EU (Ireland) Region
region	eu-west-1	ec2.eu-west-1.amazonaws.com;
# Asia Pacific (Singapore) Region
region	ap-southeast-1	ec2.ap-southeast-1.amazonaws.com;
# Asia Pacific (Tokyo) Region
region	ap-northeast-1	ec2.ap-northeast-1.amazonaws.com;
# South America (Sao Paulo) Region
region	sa-east-1	ec2.sa-east-1.amazonaws.com;

format-file "FORMATDIR/FORMATNAME.forlan";

# Ask for confirmation before running a destructive command.
confirm tty destructive;

# #####################################################################
# Sample resource maps.
#
# These maps use the Describe* EC2 requests to translate symbolic
# resource names to the corresponding IDs.  The resource name should be
# stored in the Name tag of the corresponding resource.
# #####################################################################

map "InstanceId" {
	type ec2;
	action DescribeInstances;
        arguments ("Filter.1.Name=tag:Name", "Filter.1.Value.1=${key}");
	return ".DescribeInstancesResponse.reservationSet.item.instancesSet.item.instanceId";
}

map "ImageId" {
     type ec2;
     action DescribeImages;
     arguments ("Owner.1=self",
                "Filter.1.Name=tag:Name",
                "Filter.1.Value.1=${key}");
     return ".DescribeImagesResponse.imagesSet.item.imageId";
}

map "VolumeId" {
        type ec2;
        action DescribeVolumes;
        arguments ("Filter.1.Name=tag:Name", "Filter.1.Value.1=${key}");
        return ".DescribeVolumesResponse.volumeSet.item.volumeId";
}

map "SnapshotId" {
        type ec2;
        action DescribeSnapshots;
        arguments ("Owner.1=self",
                   "Filter.1.Name=tag:Name",
                   "Filter.1.Value.1=${key}");
        return ".DescribeSnapshotsResponse.snapshotSet.item.snapshotId";
}
