#! /bin/bash
# Bash completion setup for Eclat commands.
# Copyright (C) 2012-2023 Sergey Poznyakoff
#
# Eclat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Eclat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Eclat.  If not, see <http://www.gnu.org/licenses/>.

comp_eclat() {
        # Obtain a list of command line options.  Each option occupies
	# a single line and is followed by an equals sign if it takes
	# an argument.
	local optlist=$(eclat --usage|sed '1s/^[^[]*//' |
                          tr -d '\n' |
                          tr -s ' ' |
                          sed 's/\] \[/:/g;' |
                          tr -d '[]' | 
                          awk -F: '
                           function optproc(s,   i,n,a) {
                         	if (match(s, /^-[a-zA-Z][a-zA-Z]/)) {
                         	  n = split(s, a, "");
                         	  for (i = 2; i <= n; i++)
                         	    print "-" a[i];
                         	} else if (match(s, /^-[a-zA-Z] ./)) 
                         	  print substr(s,1,2) "=";
                         	else {
                                   if (match(s, /=/)) {
                         	    opt="="
                         	    sub(/=.*/,"",s)
                         	  } else
                         	    opt=""
                         	  n = split(s, a, " ");
                                   for (i = 1; i <= n; i++) {
                         	    sub(/,$/,"",a[i])
                         	    print a[i] opt
                         	  }
                         	}
                           }
                           { for (i = 1; i < NF; i++) optproc($i) }')

	# Bail out if we're sitting on a word starting with a dash, i.e.
	# an option.  Otherwise, make sure no command appears in the command
	# line to the left of the current word.  If not, we're trying to
	# complete a command, so proceed safely.
	COMPREPLY=()
	local opt arg=${COMP_WORDS[$COMP_CWORD]}
	case $arg in
	-*) return
	esac
	for i in $(seq $(($COMP_CWORD - 1)) -1 1)
	do
		opt=${COMP_WORDS[$i]}
		case $opt in
		-*) if test -n "$arg"; then
		      if echo "$optlist" | grep -q "^$opt.*="; then
		        test $i -eq $(($COMP_CWORD - 1)) && return
		        arg=
		      else
		        return
		      fi
		    fi
		    ;;
		*)  if test -n "$arg" && test $i -lt $(($COMP_CWORD - 1)); then
		      return
		    else
		      arg=$opt
		    fi
		esac
	done
	# Finally, find matching commands.
	COMPREPLY=( $(eclat --match-commands ${COMP_WORDS[$COMP_CWORD]}) )
}
complete -F comp_eclat -o default eclat
