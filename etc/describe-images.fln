/* This file is part of Eclat.
   Copyright (C) 2013-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

if (.DescribeImagesResponse.imagesSet) {
  for (var in .DescribeImagesResponse.imagesSet.item) {
    print("\nImage ID: ", var.imageId, "\n");
    print("\tCreated on: ", var.creationDate, "\n");
    print("\tName: ", var.name, "\n");
    print("\tDescription: ", var.description, "\n");
    print("\tOwner: ", var.imageOwnerId, "\n");
    print("\tState: ", var.imageState, "\n");
    print("\tPublic: ", var.isPublic, "\n");
    print("\tType: ", var.imageType, "\n");
    print("\tArchitecture: ", var.architecture, "\n");
    if (var.kernel)
      print("\tKernel: ", var.kernelId, "\n");
    if (var.ramdiskId)
      print("\tRamdisk: ", var.ramdiskId, "\n");
    if (var.platform)
      print("\tPlatform: ", var.platform, "\n");
    print("\tVirtualization: ", var.virtualizationType, "\n");
    print("\tHypervisor: ", var.hypervisor, "\n");

    print("\tRoot Device Type: ", var.rootDeviceType, "\n");
    print("\tRoot Device Name: ", var.rootDeviceName, "\n");
    print("\tDevice mapping:\n");
    for (dev in var.blockDeviceMapping.item) {
      print("\t\t", dev.deviceName, " ");
      if (dev.virtualName)
        print(dev.virtualName);
      else
        print(dev.ebs.SnapshotId, " ",
              dev.ebs.volumeSize, " ", dev.ebs.volumeType, " ",
	      dev.ebs.deleteOnTermination);
      print("\n");
    }
    
    print("\tTag Set:\n");
    for (tag in var.tagSet.item) {
      print("\t\t", tag.key, "=", tag.value, "\n");
    }
  }
}
