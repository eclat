# This file is part of Eclat
# Copyright (C) 2012-2023 Sergey Poznyakoff
#
# Eclat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Eclat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Eclat.  If not, see <http://www.gnu.org/licenses/>.

noinst_PROGRAMS = flncat

FLNFILES=\
 allocate-address.fln\
 attach-internet-gateway.fln\
 attach-volume.fln\
 associate-address.fln\
 associate-route-table.fln\
 copy-image.fln\
 copy-snapshot.fln\
 create-image.fln\
 create-internet-gateway.fln\
 create-route.fln\
 create-route-table.fln\
 create-security-group.fln\
 create-snapshot.fln\
 create-subnet.fln\
 create-tags.fln\
 create-volume.fln\
 create-vpc.fln\
 delete-internet-gateway.fln\
 delete-route.fln\
 delete-route-table.fln\
 delete-security-group.fln\
 delete-subnet.fln\
 delete-snapshot.fln\
 delete-tags.fln\
 delete-volume.fln\
 delete-vpc.fln\
 deregister-image.fln\
 describe-addresses.fln\
 describe-availability-zones.fln\
 describe-internet-gateways.fln\
 describe-image-attribute.fln\
 describe-images.fln\
 describe-instance-attribute.fln\
 describe-instance-status.fln\
 describe-instances.fln\
 describe-regions.fln\
 describe-route-tables.fln\
 describe-security-groups.fln\
 describe-snapshot-attribute.fln\
 describe-snapshots.fln\
 describe-subnets.fln\
 describe-tags.fln\
 describe-volumes.fln\
 describe-volumes-modifications.fln\
 describe-vpc-attribute.fln\
 describe-vpcs.fln\
 detach-internet-gateway.fln\
 detach-volume.fln\
 disassociate-address.fln\
 disassociate-route-table.fln\
 get-console-output.fln\
 lsattr.fln\
 modify-image-attribute.fln\
 modify-instance-attribute.fln\
 modify-snapshot-attribute.fln\
 modify-subnet-attribute.fln\
 modify-volume.fln\
 modify-vpc-attribute.fln\
 move-address-to-vpc.fln\
 reboot-instances.fln\
 release-address.fln\
 replace-route.fln\
 restore-address-to-classic.fln\
 route.fln\
 reset-snapshot-attribute.fln\
 run-instances.fln\
 sg.fln\
 start-instances.fln\
 stop-instances.fln\
 terminate-instances.fln

EXTRA_DIST=$(FLNFILES) default.fln eclat.cfin

SUFFIXES=.cfin .conf

if SPLIT_FORMATS
SUFFIXES += .forlan .fln
.fln.forlan:
	$(AM_V_GEN)$(abs_builddir)/flncat $^ default.fln > $@
FORMATFILES = $(FLNFILES:.fln=.forlan)
FORMATNAME = $${command}
lsattr.forlan: describe-instance-attribute.fln describe-snapshot-attribute.fln
sg.forlan: sg.fln describe-security-groups.fln
else
FORMATFILES = eclat.forlan
eclat.forlan: $(FLNFILES) default.fln
	$(AM_V_GEN)(cd $(srcdir); $(abs_builddir)/flncat $(FLNFILES) default.fln) > eclat.forlan
FORMATNAME = eclat
endif

noinst_SCRIPTS=eclat.conf $(FORMATFILES)
CLEANFILES=eclat.conf $(FORMATFILES)

.cfin.conf:
	$(AM_V_GEN)sed 's^FORMATDIR^$(pkgdatadir)^;s^FORMATNAME^$(FORMATNAME)^' $< > $@

pkgdata_DATA = $(FORMATFILES)
dist_pkgdata_DATA = compl.sh

install-data-local:
	@test -z "$(DESTDIR)$(sysconfdir)" || $(mkdir_p) "$(DESTDIR)$(sysconfdir)"
	@if [ -r $(DESTDIR)$(sysconfdir)/eclat.conf ]; then :; \
	else ${INSTALL} -m 644 $(top_builddir)/etc/eclat.conf \
               $(DESTDIR)$(sysconfdir)/eclat.conf; \
        fi
