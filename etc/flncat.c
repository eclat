/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <ctype.h>
#include <assert.h>

int
comment(FILE *fp, int c, int echo)
{
	if (c != '/') {
		ungetc(c, fp);
		return 0;
	} else if (echo)
		fputc(c, stdout);
	
	if ((c = getc(fp)) == '/') {
		do
			if (echo) fputc(c, stdout);
		while ((c = getc(fp)) != EOF && c != '\n');
		return 1;
	}

	if (echo)
		fputc(c, stdout);
	
	if (c == '*') {
		do {
			do {
				c = getc(fp);
				if (c == EOF)
					return 0;
				else if (echo)
					fputc(c, stdout);
			} while (c != '*');
			c = getc(fp);
			if (echo)
				fputc(c, stdout);
		} while (c != '/');
		return 1;
	} else 

	if (!echo) {
		ungetc(c, fp);
		ungetc('/', fp);
	}
	return 0;
}

int
main(int argc, char **argv)
{
	int i;
	FILE *fp;
	int c;
	char buf[128];
	size_t lev = 0;
	int outchars = 0;
	
	for (i = 1; i < argc; i++) {
		fp = fopen(*++argv, "r");
		if (!fp) {
			perror(*argv);
			return 1;
		}

		/* Skip initial whitespace and comment lines, unless it is the
		   first file we process. */
		do {
			while ((c = fgetc(fp)) != EOF && isspace(c))
				if (i == 1) fputc(c, stdout);
		} while (c != EOF && comment(fp, c, i == 1));

		if (c != EOF) {
			if (outchars) {
				printf(" else ");
				outchars = 0;
			}

			while ((c = fgetc(fp)) != EOF) {
				outchars = 1;
				putchar(c);
				if (c == '}') {
					lev = 0;
					while ((c = fgetc(fp)) != EOF &&
					       isspace(c)) {
						assert(lev < sizeof(buf));
						buf[lev++] = c;
					}
					if (c == EOF)
						break;
					else {
						assert(lev < sizeof(buf));
						buf[lev++] = c;
						fwrite(buf, lev, 1, stdout);
					}
				}
			}
		}
		fclose(fp);
	}
	return 0;
}
