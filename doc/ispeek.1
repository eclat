.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH INSPEEK 1 "January 21, 2015" "INSPEEK" "Eclat User Reference"
.SH NAME
ispeek \- EC2 instance store lister
.SH SYNOPSIS
\fBispeek\fR\
 [\fB\-NQTdr\fR]\
 [\fB\-D\fR \fISTRING\fR]\
 [\fB\-b\fR \fIURL\fR]\
 [\fB\-p\fR \fINUMBER\fR]\
 [\fB\-\-base\-url=\fIURL\fR]\
 [\fB\-\-delimiter=\fISTRING\fR]\
 [\fB\-\-names\fR]\
 [\fB\-\-quote\fR]\
 [\fB\-\-type\fR]\
 [\fB\-\-port=\fINUMBER\fR]\
 \fIPATH\fR\
 [\fIKEY\fR...]
.br
\fBispeek\fR\
 [\fB\-Vh\fR]\
 [\fB\-\-help\fR]\
 [\fB\-\-usage\fR]\
 [\fB\-\-version\fR]
.SH DESCRIPTION
Lists contents of the EC2 instance store.  The \fIPATH\fR argument
specifies the pathname to list.  It is relative to
.BR http://169.254.169.254/latest .
If \fIPATH\fR ends with a slash, it is treated as a directory and
its content is listed.  Otherwise, it is treated as a file.
.PP
Optional \fIKEY\fR arguments are allowed when listing content of a
file.  If one or more \fIKEY\fRs are given, the file is parsed as
a JSON object.  For each \fIKEY\fR, a corresponding value is looked
up in the resulting object and printed on a separate line, prefixed
with \fIKEY\fR and a colon.
.PP
The utility must be run from a EC2 instance.
.SH OPTIONS
.TP
\fB\-b\fR, \fB\-\-base=\fIURL\fR
Base URL to use, instead of 
.BR http://169.254.169.254/latest .
.TP
\fB\-d\fR, \fB-\-debug\fR
Increase debugging level.
.TP
\fB\-p\fR, \fB\-\-port=\fINUMBER\fR
Set remote port number, instead of the default \fB80\fR.
.PP
Following option applies only when listing directories:
.TP
\fB\-r\fR, \fB\-\-recursive
List directories recursively.
.PP
The options below configure output if at least one \fIKEY\fR is given:
.TP
\fB\-D\fR, \fB\-\-delimiter=\fISTRING\fR
Delimit output values with \fISTRING\fR.  Default delimiter is a colon.
.TP
\fB\-N\fR, \fB\-\-names
Print key names.
.TP
\fB\-Q\fR, \fB\-\-quote\fR
Quote string values.  String values will be enclosed in
double-quotes.  Double-quote and backslash characters appearing within
strings will be escaped with backslashes.
.TP
\fB\-T\fR, \fB\-\-type\fR
Print type character.  Type characters are:
.BR 0 ,
for \fBnull\fR values,
.BR b ,
for booleans,
.BR n ,
for numeric values,
.BR s ,
for strings ,
.BR a ,
for arrays, and
.BR o ,
for objects.
.PP
The order of printing is: key name, type, value.
.TP
\fB\-V\fR, \fB\-\-version\fR
Print program version.
.TP
\fB\-h\fR, \fB\-\-help\fR
Give a concise help summary.
.TP
\fB\-\-usage\fR
Give a short usage message.
.SH EXAMPLES
.SS Get instance ID
.EX
$ ispeek /meta-data/instance-id
i-deadbeef
.EE
.SS Print instance data
.EX
$ ispeek /dynamic/instance-identity/document
{
  "instanceId" : "i-deadbeef",
  "billingProducts" : null,
  ...
.EE
.SS Read availability region and instance type
.EX
$ ispeek /dynamic/instance-identity/document region instanceType
eu-west-1
m3.xlarge
.EE
.SS Same, including key names and types in the output
.EX
$ ispeek -NT /dynamic/instance-identity/document region instanceType
region:s:eu-west-1
instanceType:s:m3.xlarge
.SS Recursively list the contents of \fB/meta\-data/iam\fR:
.EX
$ ispeek \-r meta\-data/iam
/meta-data/iam/info
/meta-data/iam/security-credentials/
/meta-data/iam/security-credentials/user
.EE

.SH "SEE ALSO"
.BR eclat (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
