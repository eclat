.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT 1 "November 19, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat \- EC2 Command Line Administrator Tool
.SH SYNOPSIS
.B eclat
[\fIOPTIONS\fR]
.B command
[\fIARGS\fR]
.SH DESCRIPTION
.B Eclat
is a tool that makes it possible to manage Amazon EC2 services from
the command line.  In contrast to the tools provided by Amazon itself,
.B Eclat
does not require tons of resource-consuming libraries (\fIJava
madness\fR), is very fast and efficient.
.PP
All administrative tasks are invoked through the single binary,
.BR eclat .
The task is identified by
.B command
given to it in the command line.  Options preceding the command
configure the general behavior of the tool itself.  Any options and
arguments following the command alter the behavior of that particular
command.
.PP
Upon invocation
.B eclat
reads its configuration file
.BR eclat.conf .
The default location of this file is determined when compiling the
package.  Normally it is
.BR /etc " or " /usr/local/etc .
This file provides the default configuration settings for the program,
such as the location of Amazon endpoints, default availability region,
etc.  See
.BR eclat.conf (5),
for a detailed description of its syntax.  By default this file is
preprocessed using
.BR m4 (1).
A set of command line options is provided to control this
feature (see subsection
.BR "Preprocessor control" ,
below).
.PP
Once the configuration file is read, the tool processes its command
line options.  These options, when present, modify the default
settings read from the configuration file.
.PP
Finally, the program uses its
.B command
argument to identify the action to be performed.  It then forms an
Amazon request using the rest arguments supplied to the command, and
sends it to the selected endpoint.
.PP
.I Availability region
specifies the region in the AWS where the requested resource is
located.  It can be set either in the configuration file (the
\fBdefault\-region\fR statement), or in the command line (the
\fB\-\-region\fR option).
.PP
If avaialbility region is not set, \fBeclat\fR attempts to get it
from the instance store.  This attempt will succeed only if it is
run on a EC2 instance.
.PP
An \fIendpoint\fR is a URI of the Amazon server which is supposed to
handle the request.  It is selected according to the availability
region.  The default value is provided in the configuration file
(using the \fBdefault\-endpoint\fR statement).
.PP
Upon completion of the action, Amazon sends back a
.IR response :
an XML document containing details about the result of the operation
performed.  This document is displayed using a special format, expressed
in eclat formatting language (\fBforlan\fR for short).  A set of
default formats for each request is shipped with the package.
.PP
The format to use for each particular request is selected using the
following algorithm.  If the \fB\-\-format\-expression\fR option is
given, its argument is treated as a
.B forlan
text to use as a format.  Otherwise, if the \fB\-\-format\-file\fR
option is supplied, the format to use is read from the file given as
an argument to that option.  Otherwise, if the \fB\-\-format\fR option
is used, the format is searched among the user-defined formats, using
the option argument as its name.  User-defined formats are declared in
the configuration file using the \fBdefine\-format\fR statement.
.PP
If none of these options is given, the request
.I action name
is used to look up the default format to use.  A default format is
defined in the configuration file using the \fBformat\fR statement.
.PP
Finally, if the default format is not defined as well, the format is
read from the file specified by the \fBformat\-file\fR configuration
file statement.
.PP
If
.B eclat
fails to select appropriate format using this procedure, it dumps the
contents of the response on the standard output using
.IR "path notation" ,
where each tag is identified by its name and the names of parent tags,
separated by dots.
.SH AUTHENTICATION
Requests are authenticated using a pair of strings: access key and
secret key.  Their function is similar to that of username/password in
traditional authentication schemes.  These values are obtained from
.IR "authentication provider" .
There are three types of authentication providers:
.IR immediate ,
.IR file ,
and
.IR instance\-store .
.SS Immediate Provider
Both keys are specified in the command line, using
.BR \-O " (" \-\-access\-key )
and
.BR \-W " (" \-\-secret\-key )
options.  This usage is insecure as the arguments can easily
be seen by other users (e.g. in the
.BR ps (1)
output).
.SS File Provider
The \fIfile\fR provider is requested by the following statement in
the configuration file:
.PP
.EX
authentication\-provider file \fIFILENAME\fR;
.EE
.PP
The credentials are stored in a file protected by appropriate
permissions.  Each line in such a file (named for short
\fBaccess\-file\fR) lists the access key and the corresponding secret key,
separated by a colon.  Empty lines are ignored, as well as lines
starting with a \fB#\fR sign, except as immediately followed by a
colon.  In the latter case, such a line introduces a tag, which
can be used to identify this line.  The tag consists of all the
characters following the \fB#:\fR marker up to the first whitespace
character (newline being counted as a white space).
.PP
The \fIFILENAME\fR argument is treated as a shell globbing pattern:
all files matching this pattern are attempted in turn, until a keypair
is identified, using the algorithm described below.  If an access file
cannot be opened due to insufficient privileges, no error message is
issued (unless the debugging level \fBmain.1\fR or higher is
requested).  This allows you to have different access files for use by
different groups of users.
.PP
If the \fB\-O\fR (\fB\-\-access\-key\fR) option is used, its argument
is the access key or tag to look for in the access file.  Otherwise,
.B eclat
selects the first available key pair.
.SS Instance\-store Provider
The program tries to obtain credentials from the instance store, using
the preconfigured IAM role name.
.PP
This provider type is configured by the following configuration statement:
.PP
.EX
authentication\-provider instance\-store \fIROLE\fR;
.EE
.PP
where \fIROLE\fR is the name of a IAM role.
.PP
This provider is recommended for use when you run \fBeclat\fR on an
EC2 instance which is assigned a role upon
its creation (see
.BR http://docs.aws.amazon.com/IAM/latest/UserGuide/roles-usingrole-ec2instance.html )
.SH MAPS
Each amazon resource has a unique string associated with it, called
its
.BR identifier .
Identifiers provide a means for indicating the resource to operate
upon.  For example, the command
.PP
.EX
eclat start i\-1234beef 
.EE
.PP
would start the instance \fIi\-1234beef\fR.  It can be noted, that
identifiers are rather machine- than human-oriented.  If you
have a number of resources, it is not easy to remember which
identifier corresponds to which resource.  In the example above, one
would rather prefer to identify the instance to be started by its
functional name (say, "dbserver") or its DNS name, than by its ID.  To
make this possible,
.B eclat
provides a mapping feature.
.PP
A
.B map
is a kind of dictionary that can be used to translate
human-understandable resource names to their corresponding
identifiers.  Various backends can be used for storing the
data.
.B Eclat
is shipped with the support for the following storage mechanisms:
plaintext files, GDBM databases and LDAP databases.  The actual set of
supported backends depends on the compilation options.
.PP
The mapping  feature is enabled when the \fB\-\-translate\fR
(\fB\-x\fR) command line option is used.
.PP
Maps are defined in the configuration file and are assigned unique
names.  When running a command,
.B eclat
will by default select a map named by the resource in question.  For
example, when operating on an instance, the \fIInstanceId\fR map will
be used.
.PP
For commands that support the \fBresource\-id\fR filter, identifiers
listed in that filter can be prefixed with the map name in order to
request their translation.  For example,
.PP
.EX
eclat \-x lstag resource\-id=InstanceId:webserver
.EE
.PP
will first translate the name "webserver" using the "InstanceId" map,
and then will replace the entier "InstanceId:webserver" construct with
the obtained identifier.  See also
.BR eclat\-lstag (1).
.PP
The name of the map to use can also be supplied explicitly, using the
\fI\-\-map\fR (\fI\-M\fR) option.
.PP
For a detailed description of maps, see the section
.B MAPS
in
.BR eclat.conf (5).
.SH COMMANDS
Each command is associated with two \fBidentifiers\fR, by which it can
be invoked: the \fBeclat command name\fR and the \fBEC2 command
name\fR.  Eclat command names are short strings of up to 10 letters,
which are selected to be convenient in use and consistent with the
UNIX command naming tradition.  EC2 command names are long descriptive
identifiers, consisting of letters and dashes and corresponding to the
EC2 operation codes.  These are designed for use in such contexts
where the typing shortcuts are not required, but the self-documenting
commands are preferred instead, e.g. in shell scripts.  Nevertheless,
any non\-ambiguous abbreviation can be used in place of a full EC2
command name.  Moreover, each segment (i.e. the sequence of characters
delimited by dashes), can be abbreviated independently.  For example,
\fBd\-t\fR matches \fBdescribe\-tags\fR, whereas \fBd\-i\fR matches three command names:
\fBdescribe\-instance\-attribute\fR, \fBdescribe\-instance\-status\fR and
\fBdescribe\-instances\fR.
If an ambiguous abbreviation is supplied,
.B eclat
will print a list of matching command names on the standard error and
exit with the code 64 (see the section \fBEXIT CODES\fR).
.PP
If you use
.BR bash (1),
you might consider using the command completion facility provided with
the package.  To do so, source the file
.B compl.sh
from your profile or bash startup file (either per-user or system-wide
one, at your option).  This file is installed (along with the format
files) in
.BR pkgdatadir .
For example, if your package is installed to the
.B /usr
prefix, include this line in your bash startup:
.PP
.EX
 \. /usr/share/eclat/compl.sh
.EE
.PP
With the completion facility enabled, hitting
.B TAB
after a partial command name will show you the list of possible
completions.  If there is only one completion, it will be used in
place of the partial name.
.PP
To get a help on a particular command, refer to \fBeclat\-\fIcommand\fR
(1), where \fIcommand\fR is the command name.  Currently the following
commands are implemented:
.sp
.nf
.ta 8n 20n
.ul
	Eclat name	EC2 name
.so comtab.man
.fi
.PP
Each command understands a \fB\-\-help\fR (\fB\-h\fR) command line
option which outputs a terse summary on using this particular command.
.SH OPTIONS
.SS Selecting program mode
.TP
\fB\-E\fR
Preprocess configuration and exit.
.TP
\fB\-c\fR, \fB\-\-config\-file\fR=\fIFILE\fR
Use \fIFILE\fR instead of the default configuration.
.TP
\fB\-l\fR, \fB\-\-list\-commands\fR=\fIFORMAT\fR
List all available commands using the supplied \fIFORMAT\fR.  The
format specification consists of \fIconversion specifiers\fR,
\fIescape sequences\fR and \fIordinary character sequences\fR.

A \fIconversion specifier\fR is introduced by the \fB%\fR character and
and terminated by a \fIconversion specifier character\fR.  Depending
on that character, conversion specifiers are replaced in the output with:
.sp
.nf
.ta 8n 20n
.ul
	Specifier	Expansion
	\fBn\fR	Eclat command name
	\fBi\fR	EC2 command name
.fi
.sp
Up to two additional character sequences delimited with a semicolon may
appear between the \fB%\fR and the conversion specifier character.
A sequence before the semicolon supplies the prefix string to be printed
before the expansion.  A sequence after the semicolon supplies the suffix
string, which is printed after the expansion.  If the expansion is
empty, neither prefix nor suffix appear in the output.
.sp
\fIEscape sequences\fR are two-character sequences beginning with a
backslash.  They are replaced in the output according to the table
below:
.sp
.nf
.ta 8n 18n 42n
.ul
	Sequence	Expansion	ASCII
	\fB\\\\\fR	\fB\\\fR	134
	\fB\\"\fR	\fB"\fR	042
	\fB\\a\fR	audible bell	007	
	\fB\\b\fR	backspace	010
	\fB\\f\fR	form-feed	014
	\fB\\n\fR	new line	012
	\fB\\r\fR	charriage return	015
	\fB\\t\fR	horizontal tabulation	011
	\fB\\v\fR	vertical tabulation	013
.fi
.sp
Any other character following a backslash is output verbatim.
.sp
Finally, ordinary characters are reproduced on the output as is.
.sp
This option is intended to help in generating documentation listings.
For example, the command listing above was produced using the
following command:
.sp
.in +4
.EX
eclat \-l '\\t\\\\fB%n\\\\fR\\t\\\\fB%i\\\\fR\\n' | sed 's/\-/\\\\\-/g'
.EE
.TP
\fB\-\-match\-commands\fR, \fB\-m\fR
Print matching command names and exit.  This option is intended for
use in completion facilities, such as Programmable Completion Builtins
in
.BR bash (1).
.TP
\fB\-\-test\-map\fR=\fIMAPNAME\fR
Test the translation map.  In this mode
.B eclat
treats arguments as symbolic names to be translated.  It attempts to
translate each name using the map \fIMAPNAME\fR and prints out
corresponding Amazon identifiers on the standard output.
.SS Configuration and Format Selection
.TP
\fB\-t\fR, \fB\-\-lint\fR
Parse configuration file and exit.
.TP
\fB\-F\fR, \fB\-\-format\-file\fR, \fB\-\-formfile\fR=\fIFILE\fR
Use \fIFILE\fR to format the output.
.TP
\fB\-H\fR, \fB\-\-format\fR=\fINAME\fR
Use the user-defined format \fINAME\fR for output.
.TP
\fB\-e\fR, \fB\-\-format\-expression\fR=\fIEXPR\fR
Format expression.
.SS Access credentials
.TP
\fB\-O\fR, \fB\-\-access\-key\fR=\fISTRING\fR
Set access key to use.
.TP
\fB\-W\fR, \fB\-\-secret\-key\fR=\fISTRING\fR
Set secret key to use.
.TP
\fB\-a\fR, \fB\-\-access\-file\fR=\fINAME\fR
Set access file.
.TP
\fB\-\-region\fR=\fINAME\fR
Set AWS region name.
.SS Modifiers
.TP
\fB\-N\fR, \fB\-\-no\fR
.B Eclat
can be configured to ask for confirmation before running
a potentially dangerous and unrecoverable operation (see the
section
.B CONFIRMATION
in
.BR eclat.conf (5)).
This option overrides this setting, assuming negative answer.
See also \fB\-Y\fR option below.
.TP
\fB\-s\fR, \fB\-\-sort\fR
Sort the returned XML teee prior to outputting it.
.TP
\fB\-\-ssl\fR
Use SSL (HTTPS) connection
.TP
\fB\-Y\fR, \fB\-\-yes\fR
.B Eclat
can be configured to ask for confirmation before running
a potentially dangerous and unrecoverable operation (see the
section
.B CONFIRMATION
in
.BR eclat.conf (5)).
This option overrides this setting, assuming positive answer.
See also \fB\-N\fR option above.
.SS Mapping
.TP
\fB\-M\fR, \fB\-\-map\fR=\fIMAPNAME\fR
Use this map instead of the default one (implies \fB\-\-translate\fR).
.TP
\fB\-x\fR, \fB\-\-translate\fR
Translate symbolic names encountered in the command line to the
corresponding resource IDs.
.SS Preprocessor control
.TP
\fB\-D\fR, \fB\-\-define\fR=\fISYMBOL\fR[=\fIVALUE\fB]
Define a preprocessor symbol.
.TP
\fB\-I\fR, \fB\-\-include\-directory\fR=\fIDIR\fR
Add include directory.
.TP
\fB\-\-no\-preprocessor\fR
Disable preprocessing.
.TP
\fB\-\-preprocessor\fR=\fICOMMAND\fR
Use \fICOMMAND\fR instead of the default preprocessor.
.SS Debugging
.TP
\fB\-n\fR, \fB\-\-dry\-run\fR
Do nothing, print almost everything.
.TP
\fB\-d\fR, \fB\-\-debug\fR=\fICAT\fR[.\fILEVEL\fR]
Set debugging level.
.TP
\fB\-\-dump\-grammar\-trace\fR
Dump configuration grammar traces.
.TP
\fB\-\-dump\-lex\-trace\fR
Dump lexical analyzer traces.
.TP
\fB\-p\fR, \fB\-\-check\-permissions\fR
Check if you have the required permissions for the action, without
actually making the request.  This is equivalent to
\fB\-\-add\-parameter=DryRun=true\fR.
.TP
\fB\-A\fR, \fB\-\-add\-parameter=\fINAME\fB=\fIVALUE\fR
Add parameter to the AWS request.
.SS Help and additional information
.TP
\fB\-\-config\-help
Show configuration file summary.
.TP
\fB\-V\fR, \fB\-\-version\fR
Print program version.
.TP
\fB\-h\fR, \fB\-\-help\fR
Give a concise help summary.
.TP
\fB\-\-usage\fR
Give a short usage message.
.SH "EXIT CODES"
The
.B eclat
utility indicates the success or failure of the operation by issuing a
diagnostic message and returning exit code to the shell.  The following
exit codes are used:
.TP
.BR 0 " (" EX_OK ")"
Success.
.TP
.B 16
Command cancelled.  This code is returned when the user have given
a negative answer to the confirmation request (see the section
.B CONFIRMATION
in
.BR eclat.conf (5)).
.TP
.BR 70 " (" EX_SOFTWARE ")"
Internal software error occurred.
.TP
.BR 64 " (" EX_USAGE ")"
Command line usage error, e.g. an unrecognized option has been
encountered, the command given does not correspond to any of the
known commands, or the like.
.TP
.BR 69 " (" EX_UNAVAILABLE ")"
Something went wrong.  This is a catch-all error code, used when no
other exit code is deemed suitable.
.TP
.BR 72 " (" EX_OSFILE ")"
Cannot open configuration or format file.
.TP
.BR 77 " (" EX_NOPERM ")"
Permission denied.  This code usually indicates that the configuration
file cannot be opened because of insufficient permissions.
.TP
.BR 78 " (" EX_CONFIG ")"
Error in the configuration file.
.PP
Other exit codes may be returned as a result of calling
.B exit()
from the format file.
.SH ENVIRONMENT
The environment variable
.B ECLAT_OPTIONS
can contain a list of default options for
.BR eclat .
These options are processed before the actual command line options.
.SH "SEE ALSO"
.BR eclat.conf (5),
.so xref.man
.BR forlan (5),
.BR ispeek (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2015 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

