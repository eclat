.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-LSINST 1 "January 26, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-lsinst, eclat-describe-instances \- describe available EC2 instances
.SH SYNOPSIS
\fBeclat lsinst\fR [\fIFILTER\fR...]
.PP
\fBeclat lsinst\fR \fB\-\-help\fR
.SH DESCRIPTION
This command lists your EC2 instances.
.SH FILTERS
The following filters are defined:
.TP
\fBarchitecture\fR=\fBi386\fR | \fBx86_64\fR
The architecture of the instance.
.TP
\fBavailability\-zone\fR=\fIstring\fR
The availability zone of the instance.
.TP
\fBblock\-device\-mapping.attach\-time\fR=\fIdate\fR
The date/time when an EBS volume was attached to the instance.
.TP
\fBblock\-device\-mapping.delete\-on\-termination\fR=\fIboolean\fR
Whether the device is deleted on termination of the instance.
.TP
\fBblock\-device\-mapping.device\-name\fR=\fIstring\fR
The name of the device corresponding to an Amazon EBS volume.
.TP
\fBblock\-device\-mapping.status\fR=\fIstring\fR
The status of the device.
.TP
\fBblock\-device\-mapping.volume\-id\fR=\fIstring\fR
The volume ID of the EBS volume.
.TP
\fBclient\-token\fR=\fIstring\fR
The idempotency token provided when launching the instance.
.TP
\fBdns\-name\fR=\fIstring\fR
The public DNS name of the instance.
.TP
\fBgroup\-id\fR=\fIstring\fR
The ID of a EC2 security group the instance pertains to.  For VPC
security groups, see
.BR instance.group-id .  
.TP
\fBgroup\-name\fR=\fIstring\fR
The name of a EC2 security group the instance pertains to.  For VPC
security groups, see
.BR instance.group-name .  
.TP
\fBimage\-id\fR=\fIstring\fR
The ID of the image used to create the instance.
.TP
\fBinstance\-id\fR=\fIstring\fR
The ID of the instance.
.TP
\fBinstance\-lifecycle\fR=\fBspot\fR
Matches only Spot Instances.
.TP
\fBinstance\-state\-code\fR=\fInumber\fR
An integer code representing the state of the instance.  Allowed
values for \fInumber\fR are: 0 (pending), 16 (running), 32
(shutting\-down), 48 (terminated), 64 (stopping), 80 (stopped).
.TP 
\fBinstance\-state\-name\fB=\fIstate\fR
The state of the instance.  Allowed values for \fIstate\fR are:
.BR pending ,
.BR running ,
.BR shutting\-down ,
.BR terminated ,
.BR stopping ,
.BR stopped .
.TP
\fBinstance\-type\fR=\fIstring\fR
The type of instance (e.g., \fBm1.small\fR).
.TP
\fBinstance.group\-id\fR=\fIstring\fR
The ID of a VPC security group the instance pertains to.
.TP
\fBinstance.group\-name\fR=\fIstring\fR
Tha name of a VPC security group the instance pertains to.
.TP
\fBip\-address\fR=\fIstring\fR
The public IP address of the instance.
.TP
\fBkernel\-id\fR=\fIstring\fR
The kernel ID.
.TP
\fBkey\-name\fR=\fIstring\fR
The name of the key pair used when the instance was launched.
.TP
\fBlaunch\-index\fR=\fIstring\fR
The zero-based index of the instance in the launch group, if multiple
instances were launched.
.TP
\fBlaunch\-time\fR=\fIdate\fR
Date/time when the instance was launched.
.TP
\fBmonitoring\-state\fR=\fRdisabled\fR | \fBenabled\fR
Instance monitoring state.
.TP
\fBowner\-id\fR=\fIstring\fR
The ID of the instance owner.
.TP
\fBplacement\-group\-name\fR=\fIstring\fR
The name of the placement group the instance is in.
.TP
\fBprivate\-dns\-name\fR=\fIstring\fR
Private DNS name of the instance.
.TP
\fBprivate\-ip\-address\fR=\fIstring\fR
Private IP address of the instance.
.TP
\fBproduct\-code\fR=\fIstring\fR
The product code associated with the AMI used to launch the instance.
.TP
\fBproduct\-code.type\fR=\fBdevpay\fR | \fBmarketplace\fR
The type of product code.
.TP
\fBramdisk\-id\fR=\fIstring\fR
The ID of the RAM disk.
.TP
\fBreason\fR=\fIstring\fR
The reason why the instance is in its current state.
.TP
\fBrequester\-id\fR=\fIstring\fR
The ID of the entity that launched the instance on your behalf (for
example, AWS Management Console, Auto Scaling, and so on).
.TP
\fBreservation\-id\fR=\fIstring\fR
The ID of the instance reservation.  It is created any time an
instance or a group of instances is launched.  Depending on this a
single reservation ID can correspond to a single or to multiple instances.
.TP
\fBroot\-device\-name\fR=\fIstring\fR
The name of the root device.
.TP
\fBroot\-device\-type\fR=\fBebs\fR | \fBinstance\-store\fR
The type of the root device.
.TP
\fBsource\-dest\-check\fR=\fBboolean\fR
Indicates whether the instance performs source/destination checking.
It must be \fBfalse\fR for the instance to perform NAT in a VPC. 
.TP
\fBspot\-instance\-request\-id\fR=\fIstring\fR
The ID of the Spot Instance request.
.TP
\fBstate\-reason\-code\fR=\fIstring\fR
The reason code for the state change.
.TP
\fBstate\-reason\-message\fR=\fIstring\fR
A message describing the state change.
.TP
\fBtag\-key\fR=\fIstring\fR
The key of a tag assigned to the instance, regardless of its value.  Note,
that this filter works independently of the \fBtag\-value\fR filter, i.e.
\fBtag\-key=foo tag\-value=bar\fR will match all instances that have the tag
\fBfoo\fR defined as well as those that have \fBbar\fR as a value of any
of their tags.  To request instances that have the tag \fBfoo\fR set to the
value \fBbar\fR see the \fBtag\fR:\fIKEY\fR filter below.
.TP
\fBtag\-value\fR=\fIstring\fR
The value of a tag assigned to the instance, regardless of the tag's key.
See the comment above.
.TP
\fBtag\fR:\fIkey\fR=\fIstring\fR
Selects the instances having the specified tag \fIKEY\fR set to the value
\fIVAL\fR.
.TP
\fBvirtualization\-type\fR=\fBparavirtual\fR | \fBhvm\fR
The virtualization type of the instance.
.TP
\fBhypervisor\fR=\fBovm\fR | \fBxen\fR
The hypervisor type of the instance.
.SS VPC FILTERS
The following filters are valid only if using Amazon Virtual
Private Cloud:
.TP
\fBsubnet\-id\fR=\fIstring\fR
The ID of the subnet the instance is.
.TP
\fBvpc\-id\fR=\fIstring\fR
The ID of the VPC the instance is in.
.TP
\fBnetwork\-interface.description\fR=\fIstring\fR
The description of the network interface.
.TP
\fBnetwork\-interface.subnet\-id\fR=\fIstring\fR
The ID of the subnet of the network interface.
.TP
\fBnetwork\-interface.vpc\-id\fR=\fIstring\fR
The VPC ID of the network interface.
.TP
\fBnetwork\-interface.network\-interface.id\fR=\fIstring\fR
The ID of the network interface.
.TP
\fBnetwork\-interface.owner\-id\fR=\fIstring\fR
The ID of the owner of the network interface.
.TP
\fBnetwork\-interface.availability\-zone\fR=\fIstring\fR
The availability zone of the network interface.
.TP
\fBnetwork\-interface.requester\-id\fR=\fIstring\fR
The requester ID of the network interface.
.TP
\fBnetwork\-interface.requester\-managed\fR=\fIboolean\fR
Indicates whether the network interface is being managed by an AWS
service (for example, AWSManagement Console, Auto Scaling, and so
on).
.TP
\fBnetwork\-interface.status\fR=\fBavailable\fR | \fBin\-use\fR
The status of the network interface.
.TP
\fBnetwork\-interface.mac\-address\fR=\fIstring\fR
The MAC address of the network interface.
.TP
\fBnetwork\-interface\-private\-dns\-name\fR=\fIstring\fR
The private DNS name of the network interface.
.TP
\fBnetwork\-interface.source\-destination\-check\fR=\fIBoolean\fR
Indicates whether the network interface performs source/destination
checking.  This value must be \fBfalse\fR if the interface is to
perform NAT in a VPC.
.TP
\fBnetwork\-interface.group\-id\fR=\fIstring\fR
The ID of a VPC security group associated with the network interface.
.TP
\fBnetwork\-interface.group\-name\fR=\fIstring\fR
The name of a VPC security group associated with the network interface.
.TP
\fBnetwork\-interface.attachment.attachment\-id\fR=\fIstring\fR
The ID of the interface attachment.
.TP
\fBnetwork\-interface.attachment.instance\-id\fR=\fIstring\fR
The ID of the instance to which the network interface is attached.
.TP
\fBnetwork\-interface.attachment.instance\-owner\-id\fR=\fIstring\fR
The owner ID of the instance to which the network interface is attached
.TP
\fBnetwork\-interface.addresses.private\-ip\-address\fR=\fIstring\fR
The private IP address associated with the network interface.
.TP
\fBnetwork\-interface.attachment.device\-index\fR=\fIinteger\fR
The device index to which the network interface is attached.
.TP
\fBnetwork\-interface.attachment.status\fR=\fIvalue\fR
The status of the attachment.  Allowed values are: \fBattaching\fR,
\fBattached\fR, \fBdetaching\fR, \fBdetached\fR.
.TP
\fBnetwork\-interface.attachment.attach\-time\fR=\fIdate\fR
The time when the network interface was attached to the instance.
.TP
\fBnetwork\-interface.attachment.delete\-on\-termination\fR=\fIboolean\fR
Specifies whether the attachment is deleted when an instance is terminated.
.TP
\fBnetwork\-interface.addresses.primary\fR=\fIboolean\fR
Specifies whether the IP address of the network interface is the
primary private IP address.
.TP
\fBnetwork\-interface.addresses.association.public\-ip\fR=\fIstring\fR
The ID representing the association of a VPC Elastic IP address with a
network interface in a VPC.
.TP
\fBnetwork\-interface.addresses.association.ip\-owner\-id\fR=\fIstring\fR
The owner ID of the private IP address associated with the network interface.
.TP
\fBassociation.public\-ip\fR=\fIstring\fR
The Elastic IP address bound to the network interface.
.TP
\fBassociation.ip\-owner\-id\fR=\fIstring\fR
The owner of the Elastic IP address associated with the network interface.
.TP
\fBassociation.allocation\-id\fR=\fIstring\fR
The allocation ID that AWS returned when an Elastic IP address was
allocated for the network interface.
.SH OUTPUT
Each instance is described in a separate multi-line block, between the
.B Instance:
and
.B End of instance
markers.  The information within that block is indented by one tab
stop to improve readablility.
.PP
Each instance description is prefaced by its reservation and owner IDs
and a list of groups it pertains to.  For example:
.PP
.EX
Reservation ID: r\-0ece705a
Owner ID: 053230519467
Groups:

Instance: i\-7a00642e
	Image ID: ami\-1cd4924e
	State: running
	Kernel ID: 
	Architecture: x86_64
	Private DNS: 
	Public DNS: 
	Key: VPCKey
	Type: c1.medium
	Launch Time: 2012\-06\-28T17:41:48.000Z
	Availability Zone: ap\-southeast\-1b
	Group Name: 
	Tenancy: default
	Private IP: 10.0.0.12
	Public IP: 46.51.219.63
	Groups:
		sg\-374b565b \-\- quick\-start\-3
	Root Device Type: ebs
	Root Device Name: /dev/sda1
	Device mapping:
		/dev/sda1 vol\-9e151bfc attached 2012\-06\-28T17:42:05.000Z true
	Virtualization: hvm
	Tag Set:
		Name=SingleENI
	Hypervisor: xen
	EBS Optimized: 
End of instance
.EE
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-lsiattr (1),
.BR eclat\-reboot (1),
.BR eclat\-start (1),
.BR eclat\-stop (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

