.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT.CONF 5 "March 15, 2018" "ECLAT" "Eclat User Reference"
.SH NAME
eclat.conf \- configuration file for
.BR eclat (1).
.SH DESCRIPTION
.PP
.B Eclat
configuration file consists of statements and comments.
.PP
There are three classes of lexical tokens: keywords, values, and
separators. Blanks, tabs, newlines and comments, collectively called
\fIwhite space\fR are ignored except as they serve to separate
tokens. Some white space is required to separate otherwise adjacent 
keywords and values.
.SH COMMENTS
Comments may appear anywhere where white space may appear in the
configuration file.  There are two kinds of comments:
single-line and multi-line comments.  Single-line comments start
with
.B #
or
.B //
and continue to the end of the line:
.sp
.RS 4
.EX
# This is a comment
// This too is a comment
.EE
.RE
.PP
\fIMulti-line\fB or \fIC-style\fR comments start with the two
characters
.B /*
(slash, star) and continue until the first occurrence of
.B */
(star, slash).
.PP
Multi-line comments cannot be nested.  However, single-line comments
may well appear within multi-line ones.
.SS "Pragmatic Comments"
Pragmatic comments are similar to usual single-line comments,
except that they cause some changes in the way the configuration is
parsed.  Pragmatic comments begin with a
.B #
sign and end with the next physical newline character.
.TP
.BR "#include <" "file" >
.PD 0
.TP
.BR "#include " "file"
.PD
Include the contents of the file \fIfile\fR.  If it is an
absolute file name, both forms are equivalent.  Otherwise, the form
with angle brackets searches for the file in the \fIinclude 
search path\fR, while the second one looks for it in the current working
directory first, and, if not found there, in the include search
path.
.sp
The default include search path is:
.sp
\fIprefix\fR/share/eclat/\fIVERSION\fR/include
.br
\fIprefix\fR/share/eclat/include
.sp
where \fIprefix\fR stands for the installation prefix (normally
\fB/usr\fR or \fB/usr/local\fR), and \fIVERSION\fR stands for
.B eclat
version number.
.sp
The include search path can be modified using the \fB\-I\fR
(\fB\-\-include\-directory) command line option.
.TP
.BR "#include_once <" "file" >
.PD 0
.TP
.BR "#include_once " "file"
.PD
Same as \fB#include\fR, except that, if the \fIfile\fR has already
been included, it will not be included again.
.TP
.BR "#line " "num"
.PD 0
.TP
.BR "#line " "num" " \(dq" "file" "\(dq"
.PD
This line causes the parser to believe, for purposes of error
diagnostics, that the line number of the next source line
is given by \fInum\fR and the current input file is named by
\fIfile\fR. If the latter is absent, the remembered file name
does not change.
.TP
.BR "# " "num" " \(dq" "file" "\(dq"
This is a special form of the \fB#line\fR statement, understood for
compatibility with the C preprocessor.
.PP
These statements provide a rudimentary preprocessing
features.  For more sophisticated ways to modify configuration before
parsing, see \fBPREPROCESSOR\fR.
.SH STATEMENTS
.SS "Simple statement"
A \fIsimple statement\fR consists of a keyword and value
separated by any amount of whitespace.  Simple statement is terminated
with a semicolon (\fB;\fR).
.PP
The following is a simple statement:
.sp
.RS 4
.EX
standalone yes;
pidfile /var/run/slb.pid;
.EE
.RE
.fi
.PP
A \fIkeyword\fR begins with a letter and may contain letters,
decimal digits, underscores (\fB_\fR) and dashes (\fB\-\fR).
Examples of keywords are:
.sp
.RS 4
.EX
region
format\-file
.EE
.PP
A \fIvalue\fR can be one of the following:
.TP
.I number
A number is a sequence of decimal digits.
.TP
.I boolean
A boolean value is one of the following: \fByes\fR, \fBtrue\fR,
\fBt\fR or \fB1\fR, meaning \fItrue\fR, and \fBno\fR,
\fBfalse\fR, \fBnil\fR, \fB0\fR meaning \fIfalse\fR.
.TP
.I unquoted string
An unquoted string may contain letters, digits, and any of the
following characters: \fB_\fR, \fB\-\fR, \fB.\fR, \fB/\fR,
\fB@\fR, \fB*\fR, \fB:\fR.
.TP
.I quoted string
A quoted string is any sequence of characters enclosed in
double-quotes (\fB\(dq\fR).  A backslash appearing within a quoted
string introduces an \fIescape sequence\fR, which is replaced
with a single character according to the following rules:
.sp
.nf
.ta 8n 20n
.ul
	Sequence	Replaced with
	\\a	Audible bell character (ASCII 7)
	\\b	Backspace character (ASCII 8)
	\\f	Form-feed character (ASCII 12)
	\\n	Newline character (ASCII 10)
	\\r	Carriage return character (ASCII 13)
	\\t	Horizontal tabulation character (ASCII 9)
	\\v	Vertical tabulation character (ASCII 11)
	\\\\	A single backslash
	\\\(dq	A double-quote.
.fi
.sp
In addition, the sequence \fB\\\fInewline\fR is removed from
the string.  This allows to split long strings over several
physical lines, e.g.:
.sp
.EX
    "a long string may be\\
     split over several lines"
.EE

If the character following a backslash is not one of those specified
above, the backslash is ignored and a warning is issued.

Two or more adjacent quoted strings are concatenated, which gives
another way to split long strings over several lines to improve
readability.  The following fragment produces the same result as the
example above:
.sp
.EX
    "a long string may be"
    " split over several lines"
.EE
.TP
.I Here-document
A \fIhere-document\fR is a special construct that allows to introduce
strings of text containing embedded newlines.  

The
.BI "<<" "word"
construct instructs the parser to read all the following lines up to
the line containing only \fIword\fR, with possible trailing blanks.
Any lines thus read are concatenated together into a single string.
For example: 
.sp
.EX
    <<EOT
    A multiline
    string
    EOT
.EE
.sp
The body of a here-document is interpreted the same way as a
double\-quoted string, unless \fIword\fR is preceded by a backslash
(e.g.  \fB<<\\EOT\fR) or enclosed in double\-quotes, in which case
the text is read as is, without interpretation of escape sequences.

If \fIword\fR is prefixed with \fB\-\fR (a dash), then all leading
tab characters are stripped from input lines and the line containing
\fIword\fR.  Furthermore, \fB\-\fR is followed by a single space,
all leading whitespace is stripped from them.  This allows to indent
here-documents in a natural fashion.  For example:
.sp
.EX
    <<\- TEXT
        The leading whitespace will be
        ignored when reading these lines.
    TEXT
.EE
.sp
It is important that the terminating delimiter be the only token on
its line.  The only exception to this rule is allowed if a
here-document appears as the last element of a statement.  In this
case a semicolon can be placed on the same line with its terminating 
delimiter, as in: 
.sp
.EX
    help\-text <<\-EOT
        A sample help text.
    EOT;
.EE
.TP
.I list
A comma-separated list of values, enclosed in parentheses.  The
following example shows a statement whose value is a list of strings:
.sp
.EX
    alias (test, null);
.EE
.sp
In any context where a list is appropriate, a single value is allowed
without being a member of a list: it is equivalent to a list with a
single member.  This means that, e.g.
.sp
.EX
    alias test;
.EE
.sp
is equivalent to
.sp
.EX
    alias (test);
.EE
.sp
provided that the \fBalias\fR statement is defined as taking a list as
its argument.
.TP
.I block statement
A block statement introduces a logical group of 
statements.  It consists of a keyword, followed by an optional value,
called a \fBtag\fR, and a sequence of statements enclosed in curly
braces, as shown in the example below:
.sp
.EX
    map InstanceId {
        type gdbm;
        file "instances.txt";
    }
.EE
.sp
The closing curly brace may be followed by a semicolon, although
this is not required.
.SH PREPROCESSOR
Before being parsed, the configuration file is preprocessed.
The built-in preprocessor handles only file inclusion
and
.B #line
statements, while the rest of traditional preprocessing facilities,
such as macro expansion, is supported via
.BR m4 (1),
which is used as external preprocessor.
.PP
The external preprocessor is invoked with the \fB\-s\fR flag, which
instructs it to include line synchronization information in its
output.  This information is then used by the parser to display meaningful
diagnostic.
.\" .PP
.\" An initial set of macro definitions is supplied in the file named
.\" \fBpp-setup\fR file, which is located in
.\" \fIprefix\fB/share/\fIPROGNAME\fR/\fIVERSION\fR/include}
.\" directory.
.\" .PP
.\" The default \fBpp-setup\fR file renames all m4 built-in
.\" macro names so they all start with the prefix \fBm4_\fI.  This
.\" is similar to GNU m4 \fB--prefix-builtin\fR option, but has an
.\" advantage that it works with non-GNU m4 implementations as
.\" well.
.SH MACRO EXPANSION
Arguments of some statements undergo macro expansion before use.
During the macro expansion any occurrence of \fB${NAME}\fR is replaced
by the value of macro \fBNAME\fR.  Macro names follow the usual
convention: they begin with a letter and contain letters digits and
underscores.  The curly braces around the \fBNAME\fR are optional.
They are required only if the macro reference is followed by a
character that is not to be interpreted as part of its name, as in
\fB${command}string\fR.
.PP
If a statement is eligible for macro expansion, the following macros
are always defined:
.TP
.B login
Current user login name.
.TP
.B user
Same as above, except if explicitly overridden by the user name
supplied from the command line.  In particular, this happens when
the \fBprompt\fR statement in the LDAP map declaration is set to
\fBtrue\fR (see the
.B LDAP MAP
subsection below).
.TP
.B home
Current user home directory.
.PP
Each statement can have a set of additional macros.  These are
listed in the corresponding sections below.
.SH EC2 CONFIGURATION
The statements in this group configure EC2 endpoints and regions.
.PP
A \fIregion\fR determines physical location of servers supporting
given AWS resources.
.PP
An \fIendpoint\fR is a URI of the Amazon server.  The endpoint to
use is selected based on the region name.
.TP
\fBdefault\-endpoint\fR \fIhostname\fR;
Defines the endpoint to use if no region-specific endpoint is
configured.
.TP
\fBdefault\-region\fR \fIname\fR;
Defines the name of the default region.
.TP
\fBregion\fR \fIname\fR \fIendpoint\fR;
Declares a region and the corresponding endpoint.
.PP
The region is determined by the following algorithm:
.nr step 1 1
.IP \n[step]. 
If the \fB\-\-region\fR option is given, take its argument as the
region name.
.IP \n+[step].
If the \fBdefault\-region\fR statement is defined in the configuration
file, use its argument as the region name.
.IP \n+[step].
Otherwise, attempt to obtain region from the \fIinstance store\fR.
Obviously, this step can succeed only if \fBeclat\fR is run on an
EC2 instance.
.PP
If none of these steps succeed, the program aborts.
.PP
Endpoint is selected by looking up a \fBregion\fR statement with
the \fIname\fR argument matching the currently selected region.
If such a statement is found, its \fIendpoint\fR argument defines
the endpoint to use.  Otherwise, endpoint is taken from the
\fBdefault\-endpoint\fR statement. 
.PP
An example of the EC2 endpoint configuration follows:
.PP
.EX
# Use this endpoint by default.
default\-endpoint "ec2.amazonaws.com";
# Declare what endpoints to use for each availability region:
#  US East (Northern Virginia) Region
region  us\-east\-1       ec2.us\-east\-1.amazonaws.com;
#  US West (Oregon) Region
region  us\-west\-2       ec2.us\-west\-2.amazonaws.com;
.EE
.SS HTTP METHOD AND SIGNATURE
.TP
\fBhttp\-method\fR \fIARG\fR;
Configures HTTP method.  Allowed values for \fIARG\fR are \fBGET\fR
(the default), and \fBPOST\fR.  \fIARG\fR is case-insensitive.  The
\fBPOST\fR method implies using signature version 4.
.TP
\fBsignature\-version\fR \fB2\fR|\fB4\fR;
Use the given AWS signature version algorithm.  Version \fB4\fR is
more secure and is used by default.
.SS SSL CONFIGURATION
The \fBssl\fR statement has two forms, and can be used as scalar or as
a block statement.  In scalar form it is used to enable SSL:
.TP
.B ssl yes
Enables SSL.
.PP
The block form allows for more detailed control over the SSL
configuration:
.PP
.EX
    ssl {
        enable \fIbool\fR;
        verify \fIboolean\fR;
        ca\-file \fIfile\fR;
        ca\-path \fIdirectory\fR;
    }	    
.EE
.PP
Its statements are:
.TP
\fBenable\fR \fIbool\fR
Enable or disable SSL.  This statement is equivalent to
.B ssl
in scalar form.
.TP
\fBverify\fR \fIboolean\fR
Verify peer certificate.  The default is \fByes\fR.
.TP
\fBca\-file\fR \fIfile\fR
Supplies the name of the file with CA certificates.
.TP
\fBca\-path\fR \fIdirectory\fR
Supplies the name of the directory where CA certificates are stored.
.PP
By default the CA certificates shipped with
.BR libcurl (3)
will be used.  You would rarely need to use \fBca\-file\fR or
\fBca\-path\fR statements.
.SS AUTHENTICATION
.TP
\fBauthentication\-provider\fR \fITYPE\fR [\fIARG\fR]\fB;\fR
Defines authentication provider to use.  \fIAuthentication provider\fR
is a service that supplies AWS access key ID and secret key.  See
.BR eclat (1),
section
.BR AUTHENTICATION ,
for a detailed description.

The \fITYPE\fR argument defines the provider.  Allowed values are
.BR file ,
and
.BR instance\-store .

If \fITYPE\fR is \fBfile\fR, the \fIARG\fR parameter is treated as a
shell globbing pattern: all files matching this pattern are attempted
in turn, until a keypair is found in one of them.

If \fITYPE\fR is \fBinstance\-store\fR, credentials will be obtained
from the instance store.  \fIARG\fR is optional.  If supplied, it
should be the name of the IAM role this instance is launched with.
At the time of this writing, an instance can be associated with a
single role, which will be used by default.
.TP
\fBaccess\-file\fR \fIname\fR;
This is a shortcut for \fBauthentication\-provider file \fIname\fR.
.TP
\fBsignature\-version\fR \fIN\fR;
Declares the signature version.  Valid values for \fIN\fR are \fB2\fR,
which is the default, and \fB4\fR, which provides a better security.
.SH RETRY ON ERRORS
If AWS responds with a \fBRequestLimitExceeded\fR code, \fBeclat\fR
retries the request using exponential backoff with jitter algorithm.
The algorithm is controlled by two configuration values:
.B max\-retry\-interval
and
.BR total\-retry\-timeout .
When the \fBRequestLimitExceeded\fR error is returned, \fBeclat\fR
will sleep for up to 2 seconds and then retry the request. For each
subsequent \fBRequestLimitExceeded\fR error, it will calculate the
timeout using the following formula:
.EX
   t = rand(0, min(M, 2 ** N)) + 1
.EE
where N is the attempt number, M is the value of max-retry-interval
parameter, 'rand(a,b)' selects the integer random number X such that
0 <= X <= b, and '**' is the power operator. The attempts to resend
the request will continue until either a response other than
\fBRequestLimitExceeded\fR is received (be it a response to the query or
another error response), or the total time spent in the retry loop
becomes equal to or greater than
.BR total-retry-timeout ,
whichever occurs
first.
.PP
Default values are:
.PP
.EX
max-retry-interval 600;
total-retry-timeout 1800;
.EE
.SH INSTANCE STORE CONFIGURATION
The \fBinstance\-store\fR compound statement configures HTTP access to
the instance store.  By default, \fBeclat\fR uses standard AWS values.
This statement is intended mainly as an aid in debugging:
.PP
.EX
    instance\-store {
        base\-url \fIURL\fR;
        port \fINUMBER\fR;
        document\-path \fISTRING\fR;
        credentials\-path \fISTRING\fR;
    }       
.EE
.TP
.BI base\-url " URL" ;
Base URL to use, instead of
.BR http://169.254.169.254/latest .
.TP
.BI port " NUMBER" ;
Port to use (defaults to \fB80\fR).
.TP
.BI document\-path " STRING" ;
Pathname (relative to \fBbase\-url\fR) of the instance identity document
file.  Default: 
.BR dynamic/instance-identity/document .
.TP
.BI credentials\-path " STRING" ;
Pathname (relative to \fBbase\-url\fR) of the instance store
credentials directory.  Default is
.BR meta-data/iam/security-credentials .
.SH FORMAT DEFINITIONS
This group of statements declares the formats to use.
.TP
\fBformat\fR \fIcommand\fR \fItext\fR;
Sets default format for use with \fIcommand\fR.  The \fIcommand\fR
argument is either the command name, as defined in section
.B COMMANDS
of
.BR eclat (1),
or the corresponding Amazon action name (see
http://docs.amazonwebservices.com/AWSEC2/latest/APIReference/query\-apis.html,
for the action names).  Generally speaking, command names are derived
from the corresponding action names by inserting a dash between each
capitalized part and then downcasing all capital letters.
.sp
The \fItext\fR argument is the forlan text to use as the format.  See
.BR forlan (5),
for a detailed description of the forlan syntax.
.sp
It is common practice to use the "here-document" syntax with this
statement, as shown in the example below:
.sp
.EX
format "DescribeTags" <<EOT
  if (.DescribeTagsResponse) {
    for (var in .DescribeTagsResponse.tagSet.item) {
      print(var.resourceId,"\\t",
            var.resourceType,"\\t",
            var.key,"\\t",
            var.value,"\\n");
    }
  }
EOT;
.EE
.TP
\fBdefine\-format\fR \fIname\fR \fItext\fR;
Defines a new format and assigns \fIname\fR to it.  This format can
then be used via the \fB\-\-format=\fIname\fR option.  The meaning of
the \fItext\fR parameter is the same as with the \fBformat\fR statement.
.TP
\fBformat\-file\fR \fIfilename\fR;
Read format from \fIfilename\fR.  Before using, the \fIfilename\fR
undergoes macro expansion.  The following macros are defined in
addition to the standard ones:
.RS
.TP
.B command
Replaced with the name of the
.B eclat
command being executed.
.TP
.B action
Replaced with the corresponding Amazon action name.
.RE
.in
These macros allow you to have separate format files for each EC2
request.  For example:
.sp
.EX
format\-file "/usr/share/eclat/$command.forlan";
.EE
.sp
Given this statement, the command \fBeclat lstag\fR, for
instance, will read configuration from file
\fB/usr/share/eclat/lstag.forlan\fR.
.sp
.PP
When selecting the format to use,
.B eclat
uses the following algorithm:
.PP
.nr step 1 1
.IP \n[step].
If the \fB\-\-format\-expression\fR (\fB\-e\fR) option is given, its
argument is parsed as the format text.
.IP \n+[step].
Otherwise, if the \fB\-\-format\-file\fR (\fB\-F\fR) option is given,
the format is read from the file named by its argument.
.IP \n+[step].
Otherwise, if the \fB\-\-format\fR (\fB\-H\fR) option is given, the
format is looked up among named formats (declared via the
\fBdefine\-format\fR statement), using the argument as the look-up
key.
.IP \n+[step].
Otherwise, if the default format is defined for this option (see the
\fBformat\fR configuration statement), it is used.
.IP \n+[step].
Finally, if the \fBformat\-file\fR configuration statement is used, the
format is read from the file named in its argument, after the macro
expansion.
.IP \n+[step].
If the format cannot determined by the above steps, an error is reported
and the program terminates.
.SH CONFIRMATION
Many
.B eclat
commands result in modification of your EC2 resources.  Some of them
are destructive, in the sense that such modifications cannot be undone
(e.g. deleting of a volume or termination of an instance).  To reduce
the possibility of careless usage,
.B eclat
can be configured to interactively ask for a confirmation when such a
command is requested.  This is configured by the
.B confirm
statement:
.PP
.nf
\fBconfirm\fR \fImode\fR \fBcommand\fR;
\fBconfirm\fR \fImode\fR (\fBcommand\fR[, \fBcommand\fR...]);
\fBconfirm\fR \fImode\fR \fIclass\fR;
.fi
.PP
The \fImode\fR argument specifies the requested confirmation mode.
Its valid values are:
.TP
.B tty
Ask for confirmation if the controlling terminal is a tty, i.e. if
.B eclat
is started from the command line.
.TP
.B always
Always ask for confirmation.  If the controlling terminal is not a
tty, abort the command.
.TP
.B positive
Never ask.  Assume positive confirmation.  This is the default.
.TP
.B negative
Never ask, assuming negative confirmation.
.PP
The second argument specifies the commands to which this mode is
applied.  It can be a single command name or tag, a comma-separated
list of command names or tags, or a \fBclass\fR of commands.  Valid
values for \fIclass\fR are:
.TP
.B all
All commands that modify EC2 resources.
.TP
.B destructive
Commands that destructively modify resources.
.PP
Consider the following example:
.PP
.EX
confirm tty destructive;
confirm tty (StopInstance, StartInstance);
.EE
.PP
It instructs
.B eclat
to ask for confirmation if one of the destructive commands is
requested, or if the command is start-instance or stop-instance.
.PP
Here is an example of how this modifies the behavior of
.B rmaddr
command:
.PP
.EX
$ eclat rmaddr 192.168.0.1
Proceed with release-address [Y/n] _
.EE
.PP
If the response begins with \fBY\fR (case-insensitive), it is taken
for a positive answer, and the command will be executed.  Otherwise,
.B eclat
exits returning code 16 to the shell.
.PP
The current confirmation setting can be overridden using the \fB\-Y\fR
(\fB\-\-yes\fR) or \fB\-N\fR (\fB\-\-no\fR) command line option.  The
former forces \fBpositive\fR, and the latter \fBnegative\fR
confirmation mode for the requested command, e.g.:
.PP
.EX
$ eclat -Y rmvol vol-d1234aef
.EE
.SH MAPS
Maps provide a way to translate arbitrary symbolic names to the Amazon
resource identifiers.  See the section
.B MAPS
in
.BR eclat (1),
for the discussion of the subject.  The translation is disabled by
default.  This can be changed using the following statement:
.TP
\fBtranslate\fR \fIBOOL\fR
Depending on its argument, enables or disables ID translation by
default.
.PP
A map is declared using the following statement:
.PP
.EX
\fBmap\fR \fIname\fR {
    \fBtype\fR \fIbackend\-type\fR;
    \fBkey\fR \fItrans\fR;
    /* \fIType-specific statements.\fR */
}
.EE
.PP
The \fIname\fR argument is used to identify the map.  A map can be
used from the command line using the \fB\--map=\fIname\fR option.  The
following names are special:
.TP
.B ImageId
Translates AMI names to IDs.
.TP
.B InstanceId
Translates instance names to IDs.
.TP
.B GroupId
Translates security group names to Amazon group IDs.
.TP
.B GroupName
Translates security group names to Amazon names.
.TP
.B SnapshotId
Translates snapshot names to IDs.
.TP
.B VolumeId
Translates volume names to IDs.
.TP
.B VpcId
Translates VPC names to IDs.
.TP
.B InternetGatewayId
Translates internet gateway names to IDs.
.TP
.B SubnetId
Translates subnet names to IDs.
.TP
.B RouteTableId
Translates route table names to IDs.
.TP
.B AZ
Translates availability region names to Amazon names.
.TP
.B reg
Translates region names to Amazon region names.
.PP
The \fIbackend\-type\fR declares the type of this map.  Depending on
this type, several sets of type-specific statements can be present.
These are described in detail below.
.PP
The optional
.B key
statement defines the key format.  Its argument is subject to macro
expansion.  The following macros are defined:
.TP
.B key
The original key.
.TP
.B map
The name of this map.
.SS NULL MAP
The null map always returns the key value (eventually modified using
the \fBkey\fR statement).  It is configured by setting
.sp
.EX
type null;
.EE
.sp
in the corresponding \fBmap\fR section.
.SS FILE MAP
File map is the simplest type of maps.  The data are stored in a
plain-text file.  Empty lines and lines beginning with a \fB#\fR are
ignored.  Non-empty lines contain a key and the corresponding value,
separated by a colon.  Both key and value are taken verbatim,
including any whitespace characters, if such are present.  Following
is an example of the file map, which defines two instance names:
.sp
.EX
# Database server
dbserver:i\-3456aefd
# Access server
access:i\-deadbeef
.EE
.PP
The file map is declared using the following syntax: 
.PP
.EX
    map \fIname\fR {
        type file;
        key \fItrans\fR;
        file \fIfilename\fR;
    }
.EE
.PP
The only type-specific statement is:
.TP
\fBfile\fR \fIfilename\fR
Defines the name of the file to read the map from.
.SS GDBM MAP
GDBM map, as its name implies, looks up the data in a
.BR gdbm (3)
database.  It is defined using the following statement:
.PP
.EX
    map \fIname\fR {
        type gdbm;
        key \fItrans\fR;
        file \fIfilename\fR;
        null \fIbool\fR;
    }
.EE
.TP
\fBfile\fR \fIfilename\fR
Defines the name of the database file to use.
.TP
\fBnull\fR \fIbool\fR
If set to \fBtrue\fR this statement instructs the backend to count
terminating null character as a part of key length.  By default, the null
character is not counted.
.SS SEQUENCE MAP
Maps of this type consult several other
.B eclat
maps in sequence, using return value from the previous map as a
key to the subsequent one.  The map returns the value produced by the
last map in sequence.
.PP
.EX
    map \fIname\fR {
        type sequence;
        key \fItrans\fR;
        sequence \fImaps\fR;
    }
.EE
.TP
\fBsequence\fR \fImaps\fR
This statement defines the sequence of maps to query.  Its argument is
a list of map names.  The maps it references must be declared
elsewhere in the configuration file (not necessarily before the
sequence map that references them).
.PP
For example:
.PP
.EX
    map InstanceId {
        type sequence;
        sequence (keytab, instancetab);
    }
.EE
.PP
If the sequence consists of a single element, such a map is
effectively an alias to the map named by that element:
.PP
.EX
    map i {
        type sequence;
        sequence InstanceId;
    }
.EE
.sp
.SS LDAP MAP
This map uses LDAP database to translate names.  It is defined using
the following syntax:
.sp
.EX
    map <name: string> {
        type ldap;
        key \fItrans\fR;
        ldap\-version \fInumber\fR;
        uri \fIstring\fR;
        base \fIstring\fR;
        binddn \fIstring\fR;
        bindpw \fIstring\fR;
        passfile \fIstring\fR;
        prompt \fIboolean\fR;
        tls [no | yes | only];
        filter \fIstring\fR;
        attr \fIstring\fR;
        debug \fInumber\fR;
    }
.EE
.sp
.TP
\fBldap\-version\fR \fIn\fR
Defines the LDAP version to use.  Valid values for \fIn\fR are 2 and 3
(the default).
.TP
\fBuri\fR \fIstring\fR
Defines the URI of the LDAP server.  This statement is mandatory.
.TP
\fBbase\fR \fIstring\fR
Defines the search base.
.TP
\fBbinddn\fR \fIdn\fR
Specifies the default bind DN to use when performing ldap operations.
The \fIdn\fR argument must be specified as a Distinguished Name in
LDAP format.
.sp
The argument is subject to macro expansion using the default set of macros.
.TP
\fBbindpw\fR \fIstring\fR
Specifies the password to use when performing the bind.  Use this
option only if the configuration file is protected by appropriate
permissions.  Otherwise, use \fBpassfile\fR or \fBprompt\fR
statements.
.TP
\fBpassfile\fR \fIfile\fR
Read the first line from \fIfile\fR and use it as the password.  The
newline character is stripped from the input.
.sp
The \fIfile\fR argument is subject to macro expansion, using the
default set of macros.
.TP
\fBprompt\fR \fIboolean\fR
Prompt the user for the user name and password, unless the latter is
determined using the \fBpassfile\fR statement.
.TP
\fBtls\fR \fIvalue\fR
Controls whether TLS is desired or required.  If \fIvalue\fR is
\fBno\fR (the default), TLS will not be used.  If it is \fByes\fR, the
module will issue the ``StartTLS'' command, but will continue anyway
if it fails.  Finally, if \fIvalue\fR is \fBonly\fR, TLS is mandatory,
and the backend will not establish LDAP connection unless ``StartTLS''
succeeds.
.TP
\fBfilter\fR \fIexpr\fR
Sets the LDAP filter expression.  The \fIexpr\fR should conform to the
string representation for search filters as defined in RFC 4515.
.sp
This statement is mandatory.
.TP
\fBattr\fR \fIstring\fR
Defines the attribute to use for the return value.
.sp
This statement is mandatory.
.TP
\fBdebug\fR \fIn\fR
Sets the LDAP debug number.  Refer to the OpenLDAP documentation for
the valid values of \fIn\fR and their meaning.
.SS EC2 MAP
This map makes it possible to use \fBEC2\fR services to translate
identifiers.  The idea is to query \fBEC2\fR using the symbolic ID and
obtain the real resource ID from the request.  The map is defined as
follows:
.PP
.EX
    map <name: string> {
        type ec2;
	key \fItrans\fR;
        action \fIaction-name\fR;
        arguments \fIarg-list\fR;
        return \fIreturn-path\fR;
    }
.EE
.sp
.TP
\fBaction\fR \fIaction-name\fR
Defines the \fBEC2\fR action name to use.  Refer to the Amazon API
documentation for a list of action names.
.TP
\fBarguments\fR \fIarg-list\fR
Defines the list of arguments to send with the request.  Any
occurrence of the \fB$key\fR variable within that list is replaced
with the actual key value.
.TP
\fBreturn\fR \fIreturn-path\fR
Defines the return value, using a path notation.
.PP
The example below illustrates the use of the \fBDescribeImages\fR
action to implement the \fBImageId\fR map:
.PP
.EX
    map "ImageId" {
        type ec2;
        action DescribeImages;
        arguments ("Owner.1=self",
                   "Filter.1.Name=tag:Name",
                   "Filter.1.Value.1=${key}");
        return ".DescribeImagesResponse.imagesSet.item.imageId";
    }
.EE
.SH "SEE ALSO"
.BR eclat (1),
.BR m4 (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

