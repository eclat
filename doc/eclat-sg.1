.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-SG 1 "November 19, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-sg \- manipulate security groups
.SH SYNOPSIS
.nh
.na
\fBeclat sg\fR\
 [\fB\-\-input\fR|\fB\-\-ingress\fR|\fB\-I\fR|\fB\-\-output\fR|\fB\-\-egress\fR|\fB\-O\fR]\
 \fB\-A\fR|\fB\-D\fR|\fB\-\-add\fR|\fB\-\-delete\fR [\fB\-Nn\fR]\
 [\fB\-G\fR \fINAME\fR] [\fB\-P\fR \fIPORT\fR[\fB\-\fIPORT\fR]]\
 [\fB\-g\fR \fIID\fR] [\fB\-p\fR \fIPROTO\fR] [\fB\-s\fR \fICIDR\fR]\
 [\fB\-u\fR \fIUSER\fR] [\fB\-\-group\-id\fR=\fIID\fR]\
 [\fB\-\-group\-name\fR=\fINAME\fR] [\fB\-\-name\fR]\
 [\fB\-\-next\fR, \fB\-\-new\fR]\
 [\fB\-\-port\fR=\fIPORT\fR[\fB\-\fIPORT\fR]] [\fB\-\-protocol\fr=\fIPROTO\fR]\
 [\fB\-\-source\fR=\fICIDR\fR]\
 [\fB\-\-user\fR=\fIUSER\fR] \fIGROUP\fR
.PP
\fBeclat sg\fR \fB\-\-list\fR|\fB\-L\fR [\fB\-n\fR] [\fB\-\-name\fR]\
 [\fIGROUP\fR]
.PP
\fBeclat sg\fR \fB\-h\fR
.PP
\fBeclat sg\fR \fB\-\-help\fR
.PP
\fBeclat sg\fR \fB\-\-usage\fR
.ad
.hy
.SH DESCRIPTION
The \fBeclat sg\fR command is used to list and configure \fBEC2\fR security
groups.  When invoked with the \fB\-\-list\fR (\fB\-L\fR) argument, it
displays information about the given group, or all groups in the
account, if the \fIGROUP\fR argument is not provided.  The argument is
either the group ID, or group name.  In the latter case, the
\fB\-\-name\fR (\fB\-n\fR) option should be given.
.PP
When used with \fB\-\-add\fR (\fB\-A\fR) option, the command adds the
rules to the security groups.  The rules to add are
described by the command line options that follow.  For example:
.PP
.EX
.B eclat sg --add --proto tcp --port 22 --source 192.0.2.0/24 sg-01234567
.EE
.PP
This command adds to the security group \fBsg\-01234567\fR a rule
allowing access to port \fB22\fR from IP addresses in the range
\fB192.0.2.0\fR \- \fB192.0.2.255\fR.
.PP
If \fB\-\-proto icmp\fR is used the \fB\-\-port\fR option can be
omitted.
.PP
Several rules can be added in one invocation.  The \fB\-\-next\fR
(\fB\-\-new\fR) option is used to separate them.  E.g.:
.PP
.EX
.B eclat sg --add --proto tcp --port 22 --source 192.0.2.0/24 --next \\\\
.B       --proto icmp --source 192.0.2.0/24 sg-01234567
.EE
.PP
The \fB\-\-delete\fR (\fB\-D\fR) option deletes existing rules, which
are defined using the same syntax as described above.
.PP
By default, both \fB\-\-add\fR and \fB\-\-delete\fR operate on ingress
rules.  This can be changed by placing the \fB\-\-output\fR
(\fB\-O\fR) option before them.  The \fB\-\-output\fR option remains
in effect for all options that follow it.  The \fB\-\-input option
cancels its effect.
.PP
The \fB\-\-list\fR (\fB\-L\fR) option instructs the program to list
rules in the named security group.  If no group is specified, all
existing groups will be listed.
.SH OPTIONS
.SS Modifiers
.TP
\fB\-\-input\fR, \fB\-\-ingress\fR, \fB\-I\fR
Operate on the ingress rules.
.TP
\fB\-\-output\fR, \fB\-\-egress\fR, \fB\-O\fR
Operate on the egress rules.
.PP
These modifiers apply to all \fB\-\-add\fR and \fB\-\-delete\fR
options that follow them, until another modifier or end of line is
encountered.
.PP
By default, \fB\-\-input\fR is assumed.
.PP
The \fB\-\-output\fR modifier is valid only for EC2-VPC.
.SS Commands
These options define the operation to be performed over the security
group.  A valid invocation of the \fBsg\fR subcommand must contain
exactly one of these:
.TP
.BR \-A , \-\-add
Add rules.
.TP
.BR \-D , \-\-delete
Delete rules.
.TP
.BR \-L , \-\-list
List rules.
.SS Rule constituents
The options below are used to define the rules.  Unless \fB\-\-list\fR
is requested, at least one rule must be defined.
.PP
A rule defines a set of IPv4 addresses and a port range that these are
allowed to access.  The IP addresses can be specfied either in
dotted-quad notation or as host names and can optionally be followed
by a \fB/\fR and the network mask length or the network mask.  For
example: \fB192.0.2.0/24\fR or \fB192.0.2.0/255.255.255.0\fR.  Missing
netmask part implies the network mask length of \fB32\fR.
.PP
Another way of defining IP addresses is by supplying the name or ID of
another \fBEC2\fR security group.
.TP
\fB\-G\fR, \fB\-\-group\-name=\fINAME\fR
Sets source group name.
.TP
\fB\-P\fR, \fB\-\-port\fR=\fIPORT\fR[\fB-\fIPORT\fR]
Destination port number or range.  Each \fIPORT\fR can be either a
port number in decimal or a service name from
.BR services (5).
.TP
\fB\-g\fR, \fB\-\-group\-id=\fIID\fR
Sets source group ID.
.TP
\fB\-p\fR, \fB\-\-protocol=\fIPROTO\fR
Protocol name or number.
.TP
\fB\-s\fR, \fB\-\-source=\fICIDR\fR
Source CIDR.  The argument is an IPv4 address or host name, optionally
followed by a \fB/\fR and the network mask length in decimal or the
network mask in dotted-quad notation.
.TP
\fB\-u\fR, \fB\-\-user=\fIUSER\fR
User name for the subsequent \fB\-\-group\-name\fR or
\fB\-\-group\-id\fR option.
.SS Other options
.TP
.BR \-n , \-\-name
The \fIGROUP\fR argument is a group name.  Without this option it is
treated as the group name.
.TP
.BR \-N , \-\-next , \-\-new
Begins next rule.
.SS Informational options
.TP
.BR \-h , \-\-help
Give a terse help summary.
.TP
.BR \-\-usage
List command line syntax and available options.
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-lssg (1),
.BR eclat\-mksg (1),
.BR eclat\-rmsg (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

