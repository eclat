.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-RMSNAP 1 "January 26, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-rmsnap, eclat-delete-snapshot \- delete a snapshot of an Amazon EBS volume
.SH SYNOPSIS
.nh
.na
\fBeclat rmsnap\fR \fISNAPSHOT\-ID\fR
.PP
\fBeclat rmsnap\fR \fB\-\-help\fR
.ad
.hy
.SH DESCRIPTION
This command deletes the snapshot of an Amazon EBS volume identified by
its resource ID.  If ID translation is enabled (see the section \fBMAPS\fR
in
.BR eclat (1)),
this command uses the \fBSnapshotId\fR map to translate snapshot name to
the corresponding identifier.
.SH OUTPUT
On success, the command is silent.  On error, it prints on the standard
error the error message received from Amazon.
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-lssnap (1),
.BR eclat\-mksnap (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

