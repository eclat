.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-LSSNAP 1 "January 26, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-lssnap, eclat-describe-snapshots \- describes available Amazon EBS snapshots
.SH SYNOPSIS
.nh
.na
\fBeclat lssnap\fR [\fB\-r\fR \fIID\fR] [\fB\-u \fIID\fR]\
 [\fI\-\-restorable\-by \fIID\fR]\
 [\fB\-\-owner \fINAME\fR]\
 [\fISNAPSHOT\-ID\fR...] [\fIFILTER\fR...]
.PP
\fBeclat lssnap\fR \fB\-\-help\fR
.ad
.hy
.SH DESCRIPTION
This command lists the Amazon EBS snapshots available to you.  These include
public snapshots available to any AWS account, your private snapshots and
private snapshots owned by others for which you are granted permissions.
.PP
The list of returned snapshots can be abridged using command line options,
filters and arguments.
.PP
Filters are any arguments that have the \fBkey\fR=\fBvalue\fR structure.
When filters are supplied, only those snapshots that match them are returned.
Multiple filters are joined using logical \fBAND\fR.   Multiple values
separated by commas can be specified for a given \fBkey\fR, in which case
a logical \fBOR\fR is implied.  See the section
.BR FILTERS
below for a detailed description of available filters.
.PP
Any command line argument that is not an option or filter is treated as
snapshot ID (or snapshot name, if ID mapping is enabled) to return.  Any
number of IDs can be specified.  If ID mapping is enabled (see the section \fBMAPS\fR
in
.BR eclat (1)),
this command uses the \fBSnapshotId\fR map to translate snapshot names to
the corresponding identifiers.
.PP
Snapshot IDs, filters and options can be used in conjunction.
.SH OPTIONS
.TP
\fB\-u\fR, \fB\-\-owner\fR \fIID\fR
Returns the snapshots owned by the specified owner.  The argument is 
the ID of the owner, or one of the following reserved words:
.RS
.TP
.B self
Returns the snapshots you own.
.TP
.B amazon
Returns the public AWS snapshots.
.RE
.TP
\fB\-r\fR, \fB\-\-restorable\-by\fR \fID\fR
Returns snapshots from which the specified account \fIID\fR can create volumes.
.SH FILTERS
.TP
\fBdescription\fR=\fIstring\fR
A description of the snapshot.
.TP
\fBowner\-alias\fR=\fIstring\fR
The alias of the AWS account that owns the snapshot.
.TP
\fBowner\-id\fR=\fIstring\fR
The ID of the AWS account that owns the snapshot.
.TP
\fBprogress\fR=\fIstring\fR
The progress of the snapshot, in percent.  Note that you must specify the
percent sign.
.TP
\fBsnapshot\-id\fR=\fIstring\fR
The ID of the snapshot
.TP
\fBstart\-time\fR=\fIdate\fR
The time stamp when the snapshot was initiated, in the \fIISO 8601\fR format,
i.e. \fBYYYY\fR\-\fBMM\fR\-\fBDD\fRT\fBhh\fR:\fBmm\fR:\fBss\fRZ.
.TP
\fBstatus\fR=\fBpending\fR | \fBcompleted\fR | \fBerror\fR
The snapshot status.
.TP
\fBtag\-key\fR=\fIstring\fR
The key of a tag assigned to the resource, regardless of its value.  Note,
that this filter works independently of the \fBtag\-value\fR filter, i.e.
\fBtag\-key=foo tag\-value=bar\fR will return all snapshots that have the tag
\fBfoo\fR defined and all snapshots that have \fBbar\fR as a value of any
of their tags.  To request a snapshot that has the tag \fBfoo\fR set to the
value \fBbar\fR see the \fBtag\fR:\fIKEY\fR filter below.
.TP
\fBtag\-value\fR=\fIstring\fR
The value of a tag assigned to the resource, regardless of the tag's key.
See the comment above.
.TP
\fBtag\fR:\fIKEY\fR=\fIVAL\fR
Selects the snapshots having the specified tag \fIKEY\fR set to the value
\fIVAL\fR.  For example:
.sp
.EX
tag:Name="root snapshot"
.EE
.TP
\fBvolume\-id\fR=\fIstring\fR
The ID of the volume the snapshot is for.
.TP
\fBvolume\-size\fR=\fIN\fR
The size of the volume, as a decimal number, in GiB.
.SH OUTPUT
Each line of output describes a single snapshots.  The following information
is included, separated by horizontal tab characters: the snapshot ID, the
corresponding volume ID and size in GiB, the status of the snapshot
(\fBcompleted\fR, \fBpending\fR or \fBerror\fR), progress percentage, 
the start time, and the snapshot description, if not empty.
.PP
If the snapshot has associated tags, these are listed below that line,
each tag on a separate line, prefixed by a single \fBTAB\fR character.
.PP
For example (long lines split for readability):
.PP
.EX
snap\-78a54011 vol\-4d826724 10 pending   80%  2012\-05\-07T12:51:50.000Z \
          "Daily Backup"
snap\-12345678 vol\-4d826724 10 completed	100% 2012\-04\-07T00:00:00.000Z
        comment="Taken before switching to new kernel"
        rand=E34AF890
.EE
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-rmsnap (1),
.BR eclat\-mksnap (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

