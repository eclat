.\" This file is part of Eclat -*- nroff -*-
.\" Copyright (C) 2012-2023 Sergey Poznyakoff
.\"
.\" Eclat is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Eclat is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Eclat.  If not, see <http://www.gnu.org/licenses/>.
.TH ECLAT-LSIMG 1 "January 26, 2015" "ECLAT" "Eclat User Reference"
.SH NAME
eclat-lsimg, eclat-describe-images \- describe available Amazon images
.SH SYNOPSIS
.nh
.na
\fBeclat lsimg\fR\
 [\fB\-a\fR] [\fB\-u\fR \fIID\fR]\
 [\fB\-x\fR \fIID\fR] [\fB\-\-owner\fR=\fIID\fR]\
 [\fB\-\-executable\-by\fR=\fIID\fR]\
 [\fIFILTER\fR...] [\fIAMI\fR...]
.PP
\fBeclat lsimg\fB \fB\-\-help\fR
.ad
.hy
.SH DESCRIPTION
This command lists available Amazon images.
.PP
The list of returned images can be abridged using command line options,
filters and arguments.
.PP
Filters are any arguments that have the \fBkey\fR=\fBvalue\fR structure.
When filters are supplied, only those images that match them are returned.
Multiple filters are joined using logical \fBAND\fR.   Multiple values
separated by commas can be specified for a given \fBkey\fR, in which case
a logical \fBOR\fR is implied.  See the section
.BR FILTERS
below for a list of available filters.
.PP
Any command line argument that is not an option or filter is treated as
image ID (or image name, if ID mapping is enabled) to return.  Any
number of IDs can be specified.  If ID mapping is enabled (see the section \fBMAPS\fR
in
.BR eclat (1)),
this command uses the \fBImageId\fR map to translate image names to
the corresponding identifiers.
.PP
Image IDs, filters and options can be used in conjunction.
.SH OPTIONS
.TP
\fB\-u\fR, \fB\-\-owner\fR \fIID\fR
Filters the images owned by the specified owner.  The argument is either 
the ID of the owner, or one of the following reserved words:
.RS 4
.TP
.B self
Returns the images you own.
.TP
.B amazon
Returns the public Amazon images.
.TP
.B all
Returns all available images.
.TP
.B aws\-marketplace
Returns images from the AWS marketplace.
.RE
.TP
\fB\-x\fR, \fB\-\-executable\-by\fR=\fIID\fR
Returns AMIs for which the specified user \fIID\fR has explicit
launch permissions.  The \fIID\fR is either an AWS user ID or
one of the following reserved words:
.RS 4
.TP
.B self
Returns the images which you can launch.
.TP
.B all
Returns all images.
.RE
.SH FILTERS
.TP
\fBarchitecture\fR=\fBi386\fR | \fBx86_64\fR
Selects images by architecture.
.TP
\fBblock\-device\-mapping.delete\-on\-termination\fR=\fIBOOL\fR
Selects images based on whether their Amazon EBS volume is deleted on
instance termination.
.TP
\fBblock\-device\-mapping.device\-name\fR=\fINAME\fR
The device name for the volume.
.TP
\fBblock\-device\-mapping.snapshot\-id\fR=\fIID\fR
The ID of the snapshot used for the volume.
.TP
\fBblock\-device\-mapping.volume\-size\fR=\fINUMBER\fR
The volume size in GB.
.TP
\fBblock\-device\-mapping.volume\-type\fR=\fBstandard\fR | \fBio1\fR
The type of the volume.
.TP
\fBdescription\fR=\fISTRING\fR
The textual description of the AMI.
.TP
\fBimage\-id\fR=\fIID\fR
The ID of the image.
.TP
\fBimage\-type\fR=\fBmachine\fR | \fBkernel\fR | \fBramdisk\fR
The type of the image.
.TP
\fBis\-public\fR=\fIBOOL\fR
Whether the image is public.
.TP
\fBkernel\-id\fR=\fISTRING\fR
The kernel ID.
.TP
\fBmanifest\-location\fR=\fISTRING\fR
The location of the AMI manifest.
.TP
\fBname\fR=\fISTRING\fR
The name of the AMI.
.TP
\fBowner\-alias\fR=\fISTRING\fR
The account alias.
.TP
\fBowner\-id\fR=\fISTRING\fR
The ID of the image owner.
.TP
\fBplatform=windows\fR
Selects Windows-based AMIs.
.TP
\fBproduct\-code\fR=\fISTRING\fR
.TP
\fBproduct\-code.type\fR=\fBdevpay\fR | \fBmarketplace\fR
.TP
\fBramdisk\-id\fR=\fISTRING\fR
The RAM disk ID.
.TP
\fBroot\-device\-name\fR=\fISTRING\fR
The name of the root volume.
.TP
\fBroot\-device\-type\fR=\fBebs\fR | \fBinstance\-store\fR
The name of the root device volume.
.TP
\fBstate\fR=\fBavailable\fR | \fBpending\fR | \fBfailed\fR
State of the AMI.
.TP
\fBstate\-reason\-code\fR=\fISTRING\fR
The reason code of the latest state change.
.TP
\fBstate\-reason\-message\fR=\fISTRING\fR
Message describing the reason of the latest state change.
.TP
\fBtag\-key\fR=\fISTRING\fR
The key of a tag assigned to the resource, regardless of its value.  Note,
that this filter works independently of the \fBtag\-value\fR filter, i.e.
\fBtag\-key=foo tag\-value=bar\fR will return all images that have the tag
\fBfoo\fR defined and all images that have \fBbar\fR as a value of any
of their tags.  To request the image that has the tag \fBfoo\fR set to the
value \fBbar\fR see the \fBtag\fR:\fIKEY\fR filter below.
.TP
\fBtag\-value\fR=\fISTRING\fR
The value of a tag assigned to the resource, regardless of the tag's key.
See the comment above.
.TP
\fBtag\fR:\fIKEY\fR=\fIVAL\fR
Selects images having the specified tag \fIKEY\fR set to the value
\fIVAL\fR.  For example:
.sp
.EX
tag:Name="db server"
.EE
.TP
\fBvirtualization\-type\fR=\fBparavirtual\fR | \fBhvm\fR
.TP
\fBhypervisor\fR=\fBovm\fR | \fBxen\fR
.SH OUTPUT
.SH EXAMPLE
.SH "SEE ALSO"
.BR eclat (1),
.BR eclat\-mkimg (1),
.BR eclat\-deimg (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-eclat@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2012-2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

