/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "libeclat.h"
#include <errno.h>

static struct grecs_keyword nullmap_kw[] = {
	{ "type", "'null", "Set map type", grecs_type_null },
	{ "key", "<arg: string>", "key expression", grecs_type_null },
	{ NULL }
};

static void
nullmap_confhelp()
{
	static struct grecs_keyword nullmap_top[] = {
		{ "map", "name: string",
		  "Configuration for a null map",
		  grecs_type_section, GRECS_INAC, NULL, 0, NULL, NULL,
		  nullmap_kw },
		{ NULL }
	};
	grecs_print_statement_array(nullmap_top, 1, 0, stdout);
}

static void
nullmap_free(int dbg, void *data)
{
	struct nullmap *nullmap = data;
	free(nullmap);
}

static int
nullmap_config(int dbg, struct grecs_node *node, void *data)
{
	return eclat_map_ok;
}

static int
nullmap_open(int dbg, void *data)
{
	return eclat_map_ok;
}

	
static int
nullmap_close(int dbg, void *data)
{
	return eclat_map_ok;
}

static int
nullmap_get(int dbg, int dir, void *data, const char *key, char **return_value)
{
	*return_value = grecs_strdup(key);
	return eclat_map_ok;
}

struct eclat_map_drv eclat_map_drv_null = {
	"null",
	nullmap_config,
	nullmap_open,
	nullmap_close,
	nullmap_get,
	nullmap_free,
	nullmap_confhelp
};

