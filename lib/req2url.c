/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <string.h>
#include "libeclat.h"
#include "grecs.h"

struct param_closure {
	struct grecs_txtacc *acc;
	int delim;
};

static int
add_param(void *sym, void *data)
{	
	struct ec2_param *p = sym;
	struct param_closure *pc = data;

	if (pc->delim)
		grecs_txtacc_grow_char(pc->acc, pc->delim);
	pc->delim = '&';
	grecs_txtacc_grow_string(pc->acc, p->name);
	if (p->value) {
		grecs_txtacc_grow_char(pc->acc, '=');
		grecs_txtacc_grow_string(pc->acc, p->value);
	}	
	return 0;
}

char *
eclat_request_to_url(struct ec2_request *req)
{
	struct grecs_txtacc *acc;
	char *url = NULL;
	struct param_closure pc;
	
	acc = grecs_txtacc_create();

	grecs_txtacc_grow(acc, "http", 4);
	if (req->flags & EC2_RF_HTTPS)
		grecs_txtacc_grow_char(acc, 's');
	grecs_txtacc_grow(acc, "://", 3);
	grecs_txtacc_grow(acc, req->endpoint, strlen(req->endpoint));
	grecs_txtacc_grow(acc, req->uri, strlen(req->uri));

	/* Add parameters */
	pc.acc = acc;
	pc.delim = '?';
	grecs_symtab_foreach(req->params, add_param, &pc);

	grecs_txtacc_grow_char(acc, 0);
	url = grecs_txtacc_finish(acc, 1);
	grecs_txtacc_free(acc);

	return url;
}

void
eclat_request_finalize(struct ec2_request *req)
{
	struct param_closure pc;

	if (!(req->flags & EC2_RF_POST) || req->postdata)
		return;

	eclat_request_encode(req);
	pc.acc = grecs_txtacc_create();
	pc.delim = 0;
	grecs_symtab_foreach(req->params, add_param, &pc);
	grecs_txtacc_grow_char(pc.acc, 0);
	req->postdata = grecs_txtacc_finish(pc.acc, 1);
	grecs_txtacc_free(pc.acc);
	grecs_symtab_clear(req->params);
}
