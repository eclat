/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "libeclat.h"
#include <stdio.h>
#include <sysexits.h>

int
eclat_vgetyn(int dfl, const char *prompt, va_list ap)
{
	static char *hint[] = { "y/n", "y/N", "Y/n" };
	int state = 0;
	int c, resp;
	va_list aq;

	if (dfl < -1)
		dfl = -1;
	else if (dfl > 1)
		dfl = 1;
	do {
		switch (state) {
		case 1:
			if (c == ' ' || c == '\t')
				continue;
			resp = c;
			state = 2;
			/* fall through */
		case 2:
			if (c == '\n') {
				switch (resp) {
				case 'y':
				case 'Y':
					return 1;
				case 'n':
				case 'N':
					return 0;
				case '\n':
					if (dfl >= 0)
						return dfl;
					/* fall through */
				default:
					err("Please, reply 'y' or 'n'");
				}
				state = 0;
			} else
				break;
		case 0:
			va_copy(aq, ap);
			vfprintf(stdout, prompt, aq);
			va_end(aq);
			fprintf(stdout, " [%s] ", hint[dfl+1]);
			fflush(stdout);
			state = 1;
			break;
		}
	} while ((c = getchar()) != EOF);

	exit(EX_USAGE);
}
	

int
eclat_getyn(int dfl, const char *prompt, ...)
{
	va_list ap;
	int rc;
	
	va_start(ap, prompt);
	rc = eclat_vgetyn(dfl, prompt, ap);
	va_end(ap);
	return rc;
}
	
