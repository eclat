/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include "libeclat.h"
#include "grecs.h"

void
ec2_param_free(void *ptr)
{
	struct ec2_param *p = ptr;
	free(p->name);
	free(p->value);
	free(p);
}

static int
ec2_param_dup(void *sym, void *data)
{
	struct ec2_param *p = sym;
	struct ec2_request *req = data;
	eclat_request_add_param(req, p->name, p->value);
	return 0;
}

struct ec2_request *
eclat_request_create(int flags, const char *endpoint, const char *uri,
		   char const *region, char const *access_key,
		   char const *token)
{
	struct ec2_request *req = grecs_zalloc(sizeof(*req));
	req->flags = flags;
	req->params = grecs_symtab_create(sizeof(struct ec2_param),
					NULL,
					NULL,
					NULL,
					NULL,
					ec2_param_free);
	eclat_request_add_param(req, "Version", EC2_API_VERSION);
	req->endpoint = grecs_strdup(endpoint);
	req->uri = grecs_strdup(uri);
	req->ttl = 5;
	req->region = grecs_strdup(region ? region : "us-east-1");
	req->access_key = grecs_strdup(access_key);
	req->token = token ? grecs_strdup(token) : NULL;
	return req;
}

static inline char *
safe_strdup(char const *a)
{
	return a ? grecs_strdup(a) : NULL;
}

struct ec2_request *
eclat_request_dup(struct ec2_request const *src)
{
	struct ec2_request *dst = grecs_zalloc(sizeof(*dst));
	dst->flags = src->flags;
	dst->endpoint = safe_strdup(src->endpoint);
	dst->uri = safe_strdup(src->uri);
	dst->params = grecs_symtab_create(sizeof(struct ec2_param),
					  NULL,
					  NULL,
					  NULL,
					  NULL,
					  ec2_param_free);

	grecs_symtab_foreach(src->params, ec2_param_dup, dst);
	if (src->headers == NULL) {
		dst->headers = NULL;
	} else {
		struct grecs_list_entry *ep;
		for (ep = src->headers->head; ep; ep = ep->next) {
			struct ec2_param *ent = ep->data;
			eclat_request_add_header(dst, ent->name, ent->value);
		}
	}
	dst->postdata = safe_strdup(src->postdata);
	dst->region = safe_strdup(src->region);
	dst->access_key = safe_strdup(src->access_key);
	dst->token = safe_strdup(src->token);
	dst->ttl = src->ttl;

	return dst;
}
		
