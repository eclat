/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <string.h>
#include "libeclat.h"
#include "grecs.h"

void
eclat_request_add_param0(struct ec2_request *req, const char *name,
		       const char *value, int encoded)
{
	struct ec2_param *p, key;
	int install = 1;

	key.name = (char*) name;
	p = grecs_symtab_lookup_or_install(req->params, &key, &install);
	if (!install)
		free(p->value);
	p->value = value ? grecs_strdup(value) : NULL;
	p->encoded = encoded;
}

void
eclat_request_add_param(struct ec2_request *req, const char *name, 
                        const char *value)
{
	eclat_request_add_param0(req, name, value, 0);
}

void
eclat_request_add_param_encoded(struct ec2_request *req, const char *name,
			        const char *value)
{
	if (value) {
		char *str;
		urlencode(value, strlen(value), &str, NULL);
		eclat_request_add_param0(req, name, str, 1);
		free(str);
	} else
		eclat_request_add_param0(req, name, value, 1);
}

	
static void
free_header(void *data)
{
	struct ec2_param *p = data;
	free(p->name);
	free(p->value);
	free(p);
}

void
eclat_request_add_header(struct ec2_request *req, const char *name, 
                         const char *value)
{
	struct ec2_param *ent;
	
	if (!req->headers) {
		req->headers = grecs_list_create();
		req->headers->free_entry = free_header;
	}
	ent = grecs_malloc(sizeof(*ent));
	ent->name = grecs_strdup(name);
	if (value) {
		int len;
		
		while (*value == ' ' || *value == '\t')
			++value;
		len = strlen(value);
		while (len && (value[len-1] == ' ' || value[len-1] == '\t'))
			--len;
		ent->value = grecs_malloc(len + 1);
		memcpy(ent->value, value, len);
		ent->value[len] = 0;
	} else
		ent->value = NULL;
	grecs_list_append(req->headers, ent);
}
