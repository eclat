/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "libeclat.h"
#include <sysexits.h>

static size_t
acc_cb(void *ptr, size_t size, size_t nmemb, void *data)
{
	size_t realsize = size * nmemb;
	struct grecs_txtacc *acc = data;
	grecs_txtacc_grow(acc, ptr, realsize);
	return realsize;
}

CURL *
instance_store_curl_new(struct grecs_txtacc *acc)
{
	CURL *curl;
	
	curl = curl_easy_init();
	if (!curl)
		die(EX_UNAVAILABLE, "curl_easy_init failed");
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, acc_cb);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, acc);
	return curl;
}

int
instance_store_read(const char *url, CURL *curl)
{
	CURLcode res;
	long http_resp;
	
	curl_easy_setopt(curl, CURLOPT_URL, url);
		
	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		die(EX_UNAVAILABLE, "CURL: %s", curl_easy_strerror(res));

	res = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_resp);
	if (res != CURLE_OK)
		die(EX_UNAVAILABLE, "CURL: %s", curl_easy_strerror(res));
	
	switch (http_resp) {
	case 200:
		break;

	case 404:
		return -1;

	default:
		die(EX_UNAVAILABLE, "CURL: got response %3d, url %s",
		    http_resp, url);
	}
	return 0;
}
