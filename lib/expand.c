/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "libeclat.h"
#include "wordsplit.h"
#include <sysexits.h>
#include <termios.h>
#include <unistd.h>
#include <pwd.h>

char *
eclat_expand_kw(const char *input, const char **ukw)
{
	struct wordsplit ws;
	char *ret;
	static char **defkw;
	static size_t defkwsize = 6;
	const char **kw;
	size_t kwsize;
	int i;
	
	if (!defkw) {
		uid_t uid = getuid();
		struct passwd *pw = getpwuid(uid);
		if (!pw)
			die(EX_UNAVAILABLE,
			    "cannot determine user name (uid=%lu)",
			    (unsigned long) uid);
		defkw = grecs_calloc(defkwsize, sizeof(defkw[0]));
		defkw[0] = "login";
		defkw[1] = grecs_strdup(pw->pw_name);
		defkw[2] = "user";
		defkw[3] = grecs_strdup(pw->pw_name);
		defkw[4] = "home";
		defkw[5] = grecs_strdup(pw->pw_dir);
	}

	if (ukw) {
		for (i = 0; ukw[i]; i++);
		kwsize = defkwsize + i + 1;
	} else
		kwsize = defkwsize + 1;
	kw = grecs_calloc(kwsize, sizeof(kw[0]));
	i = 0;
	if (ukw)
		for (; ukw[i]; i++)
			kw[i] = ukw[i];
	for (; i < defkwsize; i++)
		kw[i] = defkw[i];
	kw[i] = NULL;
	
	ws.ws_env = kw;
	ws.ws_error = err;
	if (wordsplit(input, &ws,
		      WRDSF_NOSPLIT | WRDSF_NOCMD |
		      WRDSF_ENV | WRDSF_ENV_KV | WRDSF_WARNUNDEF | WRDSF_ERROR))
		die(EX_SOFTWARE, "error expanding pattern %s: %s",
		    input, wordsplit_strerror(&ws));
	
	free(kw);
	ret = ws.ws_wordv[0];
	ws.ws_wordv[0] = NULL;
	wordsplit_free(&ws);
	return ret;
}
