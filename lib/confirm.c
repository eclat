/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include "libeclat.h"
#include <unistd.h>
#include <sysexits.h>

int
eclat_confirm(enum eclat_confirm_mode mode, const char *prompt, ...)
{
	int rc;
	va_list ap;

	switch (mode) {
	case eclat_confirm_unspecified:
	case eclat_confirm_positive:
		return 1;
	case eclat_confirm_negative:
		return 0;
	case eclat_confirm_tty:
		if (!isatty(0))
			return 1;
		break;
	case eclat_confirm_always:
		if (!isatty(0))
			die(EX_USAGE,
			    "confirmation required, "
			    "but stdin is not a terminal");
		break;
	}
	
	va_start(ap, prompt);
	rc = eclat_vgetyn(-1, prompt, ap);
	va_end(ap);
	
	return rc;
}

		
