/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <string.h>
#include "libeclat.h"
#include "grecs.h"

static int
encode_param(void *sym, void *data)
{	
	struct ec2_param *p = sym;
	char *enc;

	if (!p->encoded && p->value) {
		urlencode(p->value, strlen(p->value), &enc, NULL);
		free(p->value);
		p->value = enc;
		p->encoded = 1;
	}
	return 0;
}

void
eclat_request_encode(struct ec2_request *req)
{
	grecs_symtab_foreach(req->params, encode_param, NULL);
}

