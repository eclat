/* This file is part of Eclat.
   Copyright (C) 2012-2023 Sergey Poznyakoff.
 
   Eclat is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Eclat is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Eclat.  If not, see <http://www.gnu.org/licenses/>. */

#include <stddef.h>
#include <expat.h>
#include <curl/curl.h>
#include "grecs.h"

extern const char *program_name;

struct debug_category {
	const char *name;
	size_t length;
	int level;
};

extern struct debug_category debug_category[];
#define LIBECLAT_DBG_MAX 64

#define debug_level(cat) ((cat >= 0 && cat < LIBECLAT_DBG_MAX) ?	\
			   debug_category[cat].level : 0)
#define debug(cat, lev, s)					\
	do {							\
		if (debug_level(cat) >= (lev))			\
			debug_printf s;				\
	} while(0)

void set_program_name(const char *arg);

void die(int status, const char *fmt, ...);
void vdiag(grecs_locus_t const *locus, const char *qual, const char *fmt,
	   va_list ap);
void diag(grecs_locus_t const *locus, const char *qual, const char *fmt, ...);
void err(const char *fmt, ...);
void warn(const char *fmt, ...);
void debug_printf(const char *fmt, ...);

int parse_debug_level(const char *arg);
int debug_register(char *name);

int eclat_trace_fun(CURL *handle, curl_infotype type,
		    char *data, size_t size,
		    void *userp);
void eclat_set_curl_trace(CURL *curl, int lev);

char *path_concat(const char *dir, const char *comp);
CURL *instance_store_curl_new(struct grecs_txtacc *acc);
int instance_store_read(const char *url, CURL *curl);


void hmac_sha1(const void *text, size_t textlen,
	       const void *key, size_t keylen,
	       void *digest);
void hmac_sha256(const void *text, size_t textlen,
		 const void *key, size_t keylen,
		 void *digest);

void urlencode(const char *input, size_t len,
	       char **poutput, size_t *poutlen);

void eclat_base64_encode(const unsigned char *input, size_t input_len,
			 unsigned char **output, size_t *output_len);
int eclat_base64_decode(const unsigned char *input, size_t input_len,
			unsigned char **output, size_t *output_len);


struct ec2_param {
	char *name;
	char *value;
	int encoded;
};

#define EC2_API_VERSION "2016-11-15"

#define EC2_RF_HTTPS 0x01
#define EC2_RF_POST  0x02

struct ec2_request {
	int flags;       /* Composed from EC2_RF_* bits */
	char *endpoint;  /* endpoint  */
	char *uri;       /* URI without parameters */
	struct grecs_symtab *params; /* Query parameters */
	struct grecs_list *headers; /* Query headers */
	char *postdata;
	char *region;
	char *access_key;
	char *token;
	unsigned long ttl; /* Time-to-live in seconds */
};

struct ec2_request *eclat_request_create(int flags, const char *endpoint,
				     const char *uri, char const *region,
				     char const *access_key, char const *token);
struct ec2_request *eclat_request_dup(struct ec2_request const *src);
void eclat_request_free(struct ec2_request *);
void eclat_request_add_param0(struct ec2_request *req, const char *name,
			      const char *value, int encoded);
void eclat_request_add_param(struct ec2_request *req, const char *name,
			   const char *value);
void eclat_request_add_param_encoded(struct ec2_request *req, const char *name,
				   const char *value);
void eclat_request_add_header(struct ec2_request *req, const char *name,
			    const char *value);

void eclat_request_sign(struct ec2_request *req, char *secret, char *version);

char *eclat_request_to_url(struct ec2_request *req);

void eclat_request_encode(struct ec2_request *req);
void eclat_request_finalize(struct ec2_request *req);

void ec2_param_free(void *ptr);

struct grecs_list *ec2_param_list_create(void);
void ec2_param_list_append(struct grecs_list **lp, char const *name,
			   char const *val);
void eclat_request_add_param_list(struct ec2_request *req,
				  struct grecs_list *lp);

typedef struct eclat_partial_tree *eclat_partial_tree_t;

eclat_partial_tree_t eclat_partial_tree_create(void);
void eclat_partial_tree_destroy(eclat_partial_tree_t);
struct grecs_node *eclat_partial_tree_finish(eclat_partial_tree_t);
void eclat_partial_tree_data_handler(void *data, const XML_Char *s, int len);
void eclat_partial_tree_start_handler(void *data, const XML_Char *name,
				      const XML_Char **atts);
void eclat_partial_tree_end_handler(void *data, const XML_Char *name);

void eclat_trimnl(char *s);
char *eclat_expand_kw(const char *input, const char **ukw);
char *eclat_getans(char *prompt, char *dfl, int pass);
int eclat_getyn(int dfl, const char *prompt, ...);
int eclat_vgetyn(int dfl, const char *prompt, va_list ap);

enum eclat_confirm_mode
{
	eclat_confirm_unspecified,
	eclat_confirm_positive,
	eclat_confirm_negative,
	eclat_confirm_tty,
	eclat_confirm_always
};

int eclat_confirm(enum eclat_confirm_mode mode, const char *prompt, ...);


#define ECLAT_MAP_OPEN     0x01

struct eclat_map_drv {
	const char *name;
	int (*map_config)(int, struct grecs_node *, void *);
	int (*map_open)(int, void *);
	int (*map_close)(int, void *);
	int (*map_get)(int, int, void *, const char *, char **);
	void (*map_free)(int, void *);
	void (*map_confhelp)(void);
};

struct eclat_map {
	char *name;
	struct grecs_locus locus;
	char *keytrans;
	struct eclat_map_drv *drv;
	void *data;
	int flags;
};

enum eclat_map_status {
	eclat_map_ok,
	eclat_map_failure,
	eclat_map_bad_dir,
	eclat_map_not_found
};

#define MAP_DIR 0
#define MAP_REV 1

void eclat_map_init(void);
struct eclat_map *eclat_map_lookup(const char *name);
int eclat_map_config(struct grecs_node *node, struct eclat_map **return_map);
void eclat_map_free(struct eclat_map *map);
int eclat_map_open(struct eclat_map *map);
int eclat_map_close(struct eclat_map *map);
int eclat_map_get(struct eclat_map *map, int dir, const char *key,
		  char **value);
const char *eclat_map_strerror(int rc);
int eclat_map_drv_register(struct eclat_map_drv *drv);
void eclat_map_foreach(int (*fun)(struct eclat_map *, void *), void *data);
void eclat_map_free_all(void);
void eclat_map_confhelp(void);

int eclat_map_name_split(const char *mapname, char **name, char **endp);

int eclat_get_string_node(struct grecs_node *node, const char *name,
			  int optional,
			  struct grecs_node **pret);

extern struct eclat_map_drv eclat_map_drv_file;
extern struct eclat_map_drv eclat_map_drv_gdbm;
extern struct eclat_map_drv eclat_map_drv_ldap;
extern struct eclat_map_drv eclat_map_drv_null;
extern struct eclat_map_drv eclat_map_drv_seq;
extern struct eclat_map_drv eclat_map_drv_bidi;
